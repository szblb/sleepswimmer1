--Run these lines (F8) to clear all the data from all the tables
DELETE FROM "sleepswimmer1.db::activity";
DELETE FROM "sleepswimmer1.db::class_5min";
DELETE FROM "sleepswimmer1.db::coachdailysurvey";
DELETE FROM "sleepswimmer1.db::hr_5min";
DELETE FROM "sleepswimmer1.db::hypnogram_5min";
DELETE FROM "sleepswimmer1.db::met_1min";
DELETE FROM "sleepswimmer1.db::readiness";
DELETE FROM "sleepswimmer1.db::rmssd_5min";
DELETE FROM "sleepswimmer1.db::sleep";
DELETE FROM "sleepswimmer1.db::user";
DELETE FROM "sleepswimmer1.db::yourdailysurvey1";
DELETE FROM "sleepswimmer1.db::yourdailysurvey2";
DELETE FROM "sleepswimmer1.db::yourdailysurvey3";