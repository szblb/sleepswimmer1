sap.ui.define([
	'sap/m/MessageBox',
	'sap/ui/core/Fragment',
	"swimmer_ui/swimmer_ui/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	'sap/m/MessageToast',
	'sap/ui/unified/DateRange'],
function (MessageBox, Fragment, BaseController, JSONModel, MessageToast,DateRange) {
	"use strict";

	return BaseController.extend("swimmer_ui.swimmer_ui.controller.editprofilesw", {

		onInit: function () {
			sap.ui.core.UIComponent.getRouterFor(this).getRoute("editprofilesw").attachPatternMatched(this._onRouteMatched,this);
		},
		onNavButtonPressed: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepagesw");
		},
		
		_onRouteMatched: function(){
			var response = jQuery.ajax({
				url: "/swimmer_java_api/getUserInfo",
				method: "GET",
				async: false
			}).responseText;
			var json = JSON.parse(response);
			
			var oView = this.getView();
			oView.byId("name").setValue(json["name"]);
			oView.byId("lname").setValue(json["lastName"]);
			oView.byId("birthdayLabel").setText(json["birthday"]);
			oView.byId("gender").setSelectedKey(json["gender"]);
			oView.byId("height").setValue(json["height"]);
			oView.byId("weight").setValue(json["weight"]);
			oView.byId("stroke").setValue(json["specialityStroke"]);
			oView.byId("sdistance").setValue(json["specialityDistance"]);
			oView.byId("srs").setValue(json["specialityReferenceStrokes"]);
		},
		
		handleSaveMessageBoxPress: function(oEvent) {
			var oView = this.getView();
			var name = oView.byId("name").getValue();
			var lname = oView.byId("lname").getValue();
			var bday = new Date(oView.byId("calendar").getSelectedDates()[0].getStartDate());
			var gender = oView.byId("gender").getSelectedItem().getKey();
			var height = oView.byId("height").getValue();
			var weight = oView.byId("weight").getValue();
			var stroke = oView.byId("stroke").getValue();
			var sdistance = oView.byId("sdistance").getValue();
			var srs = oView.byId("srs").getValue();
			var month;
			var day;
			if(bday.getMonth() < 9) {
				month = "0" + (bday.getMonth() + 1);
			} else {
				month = bday.getMonth() + 1;
			}
			if(bday.getDate() <= 9) {
				day = "0" + bday.getDate();
			} else {
				day = bday.getDate();
			}
			var birthday = bday.getFullYear() + "-" + month + "-" + day;
			var userInfo = {
				name: name,
				lastName: lname,
				birthday: birthday,
				gender: gender,
				height: height,
				weight: weight,
				specialityStroke: stroke,
				specialityDistance: sdistance,
				specialityReferenceStrokes: srs
			};

			jQuery.ajax({
				url: "/swimmer_java_api/setUserInfo/?name=" + name + "&lastName=" + lname + "&birthday=" + birthday
					 + "&gender=" + gender + "&height=" + height + "&weight=" + weight + "&specialityStroke=" + stroke
					  + "&specialityDistance=" + sdistance + "&specialityReferenceStrokes=" + srs,
				method: "GET",
				async: false
			});
			
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepagesw");
			
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.success(
				"Your profile has been saved successfully!",
				{
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		},
		allowOura: function() {
			if(this.checkIfUserAllowedOura() === true) {
					this.showMessageToast("You already allowed access to Oura API!");
				} else {
					this.onOpenOuraAllowDialog();
				}
		},
		
		onExit: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepagesw");
		}
	});
});