sap.ui.define([
	"swimmer_ui/swimmer_ui/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("swimmer_ui.swimmer_ui.controller.OuraNotConnected", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.OuraConnected
		 */
		onInit: function () {
			this.getRouter().getRoute("ouraConnected").attachPatternMatched(this._onRouteMatched,this);
		},
		
		_onRouteMatched: function(oEvent){
		}

	});

});