sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("swimmer_ui.swimmer_ui.controller.View1", {
		onInit: function () {
			var oModel = new sap.ui.model.json.JSONModel({
		      SwimmersList : [
			    {
		          "Name": "Claire DUPONT",
		          "Gender": "F",
		          "Age": "14",     
		          "Height": "161m",
		          "Weight": "50", 
		          "Stroke_Speciality": "Backstroke",
		          "Status": "Active"
		          
		        },
		         {
		          "Name": "Marie-Anne TRUFFAUT",
		          "Gender": "F",
		          "Age": "18",     
		          "Height": "170m",
		          "Weight": "50", 
		          "Stroke_Speciality": "Breaststroke",
		          "Status": "Inactive"
		          
		        },
		        {
		          "Name": "Adrien LEBEL",
		          "Gender": "M",
		          "Age": "11",     
		          "Height": "195m",
		          "Weight": "85", 
		          "Stroke_Speciality": "Freestyle",
		          "Status":"Active"
		        },
		        {
		          "Name": "Téophane BIDDAUT",
		          "Gender": "M",
		          "Age": "17",     
		          "Height": "185m",
		          "Weight": "70", 
		          "Stroke_Speciality": "Butterfly",
		          "Status":"Active"
		        },
		         {
		          "Name": "Amélie PIFFAUT",
		          "Gender": "F",
		          "Age": "23",     
		          "Height": "175m",
		          "Weight": "65", 
		          "Stroke_Speciality": "Freestyle",
		          "Status":"Inactive"
		        },
		         {
		          "Name": "Tancrède HAYER",
		          "Gender": "M",
		          "Age": "20",     
		          "Height": "185m",
		          "Weight": "80", 
		          "Stroke_Speciality": "Breaststroke",
		          "Status":"Inactive"
		        },
		         {
		          "Name": "Quentin BAJOT",
		          "Gender": "M",
		          "Age": "16",     
		          "Height": "185m",
		          "Weight": "75", 
		          "Stroke_Speciality": "Butterfly",
		          "Status":"Active"
		        }
		        
		      ]
		    });
		    oModel.setDefaultBindingMode("TwoWay");
		    this.getView().setModel(oModel);
		    
	},
	onCreate: function(){
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oRouter.navTo("swimmerprofile");
	},
	onReturn: function(){
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oRouter.navTo("homepage");
	},
	  onDelete : function(oEvent){
      var sBindingPath = oEvent.getSource().getParent().getBindingContext().getPath();
      console.log(sBindingPath);
      var selectedIndex = sBindingPath.split("/")[2];
      var oTableData = this.getView().byId("idProductsTable").getModel().getData();
      console.log(oTableData);
      oTableData.results.splice(selectedIndex, 1);
      this._oTable.getModel().setData(oTableData);
    }
    
	});
});