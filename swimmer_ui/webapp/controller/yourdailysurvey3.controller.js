sap.ui.define([
	"sap/m/MessageBox",
	"sap/ui/core/mvc/Controller"
], function (MessageBox, Controller) {
	"use strict";

	return Controller.extend("swimmer_ui.swimmer_ui.controller.yourdailysurvey3", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.yourdailysurvey3
		 */
		onInit: function () {
			var oRouter=sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("yourdailysurvey3").attachPatternMatched(this._onRouteMatched,this);
		},
		
		_onRouteMatched: function(oEvent) {
			this.surveyDay = oEvent.getParameter("arguments").surveyDay;
		},
		
		onSave: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var oView = this.getView();
			
			var litersOfWater = oView.byId("litersOfWater").getValue();
			var fruitsAndVegetables = oView.byId("fruitsAndVegetables").getValue();
			var proteins = oView.byId("proteins").getValue();
			var carbs = oView.byId("carbs").getValue();
			var snacks = oView.byId("snacks").getValue();
			
			var response = jQuery.ajax({
				url: "/swimmer_java_api/setUserDailySurvey3/?surveyDay=" + this.surveyDay + "&litersOfWater=" + litersOfWater 
					 + "&fruitsAndVegetables=" + fruitsAndVegetables + "&proteins=" + proteins + "&carbs=" + carbs
					 + "&snacks=" + snacks,
				method: "GET",
				async: false
			}).responseText;
			
			if(response === "NOK") {
				var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				MessageBox.success(
					"You already completed a survey for that day!",
					{
						styleClass: bCompact ? "sapUiSizeCompact" : ""
					}
				);
			} else {
				MessageBox.success(
					"Thanks for completing the entire survey!",
					{
						styleClass: bCompact ? "sapUiSizeCompact" : ""
					}
				);
				oRouter.navTo("homepagesw");
			}
		}

	});

});