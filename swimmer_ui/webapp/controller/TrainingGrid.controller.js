sap.ui.define(['jquery.sap.global',
		'sap/m/MessageBox',
		'sap/m/Button',
		'sap/m/Dialog',
		'sap/m/Label',
		'sap/m/Popover',
		'sap/m/List',
		'sap/m/StandardListItem',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel'],
	function(MessageBox, jQuery, Button, Dialog, Label, Popover, List, StandardListItem, Fragment, Controller, JSONModel) {
		"use strict";

		var PageController = Controller.extend("sap.m.sample.PlanningCalendarModifyAppointments.Page", {

			onInit: function () {
				var oModel = new JSONModel();
				oModel.setData({
					startDate: new Date("2017", "0", "15", "8", "0"),
					schedule:[
						{
						name:"Intensive week"
					},
					{
						name:"Light week"
					},
					{
						name:"Championship week"
					}
					],
					swimmers:[
						{
							name:"Claire DUPONT"
						},
						{	
							name: "Marie-Anne TRUFFAUT"
						},
						{
							name: "Adrien LEBEL"
						},
						{
							name: "Téophane BIDDAUT"
						},
						{
							name: "Amélie PIFFAUT"
						},
						{
							name: "Tancrède HAYER"
						},
						{
							name: "Quentin BAJOT"
						}
						],
				
					people: [
						{
							pic: "sap-icon://employee",
							name: "Affa MAROUA",
							role: "coach",
							appointments: [
									{
								start: new Date("2019", "0", "8", "08", "30"),
								end: new Date("2017", "0", "8", "09", "30"),
								title: "Light Training",
								type: "Type07",
								tentative: false
							},
							{
								start: new Date("2019", "0", "11", "10", "00"),
								end: new Date("2017", "0", "11", "12", "00"),
								title: "Light Training",
								type: "Type07",
								tentative: false
							},
							{
								start: new Date("2019", "0", "12", "11", "30"),
								end: new Date("2017", "0", "12", "13", "30"),
								title: "Intensive Training",
								type: "Type01",
								tentative: true
							},
							{
								start: new Date("2019", "0", "15", "08", "30"),
								end: new Date("2017", "0", "15", "14", "30"),
								title: "Championship",
								location: "Rennes",
								type: "Type02",
								tentative: false
							},
							{
								start: new Date("2019", "0", "15", "13", "30"),
								end: new Date("2017", "0", "15", "17", "30"),
								title: "Swimming Training",
								info: "Mandatory",
								type: "Type07",
								tentative: false
							},
							{
								start: new Date("2019", "0", "18", "08", "30"),
								end: new Date("2017", "0", "18", "09", "30"),
								title: "Fitness Training",
								type: "Type05",
								tentative: false
							},
							{
								start: new Date("2019", "0", "18", "13", "00"),
								end: new Date("2017", "0", "18", "14", "30"),
								title: "Swimmming Training",
								info: "regular",
								type: "Type07",
								tentative: false
							},
							{
								start: new Date("2016", "0", "21", "08", "30"),
								end: new Date("2017", "0", "21", "09", "30"),
								title: "Championship",
								location:"Rouen",
								type: "Type02",
								tentative: true
							},
							{
								start: new Date("2016", "0", "30", "08", "30"),
								end: new Date("2017", "0", "30", "09", "30"),
								title: "Fitness Training",
								info: "Mandatory",
								type: "Type05",
								tentative: false
							},
							{
								start: new Date("2016", "0", "30", "10", "0"),
								end: new Date("2017", "0", "30", "12", "0"),
								title: "Fitness Training",
								info: "Mandatory",
								type: "Type05",
								tentative: false
							},
							{
								start: new Date("2016", "0", "30", "13", "30"),
								end: new Date("2017", "0", "30", "17", "30"),
								title: "Swimming Training",
								info: "Mandatory",
								type: "Type07",
								tentative: false
							},
							{
								start: new Date("2016", "0", "31", "10", "00"),
								end: new Date("2017", "0", "31", "11", "30"),
								title: "Fitness Training",
								info: "Mandatory",
								type: "Type05",
								tentative: false
							},
							{
								start: new Date("2016", "1", "4", "10", "0"),
								end: new Date("2017", "1", "4", "12", "0"),
								title: "Swimming Training",
								info: "Mandatory",
								type: "Type07",
								tentative: false
							},
							{
								start: new Date("2016", "2", "30", "10", "0"),
								end: new Date("2017", "4", "33", "12", "0"),
								title: "Championship",
								location:"Paris",
								type: "Type02",
								tentative: false
							}
						],
							headers: [
								{
									start: new Date("2017", "0", "16", "09", "00"),
									end: new Date("2017", "0", "16", "10", "30"),
									title: "Team Meeting",
									type: "Type05",
									info: "Mandatory"
								}
							]
						}
					]
				});
				this.getView().setModel(oModel);

			},

			handleAppointmentSelect: function (oEvent) {
				var oAppointment = oEvent.getParameter("appointment");

				if (oAppointment) {
					this._handleSingleAppointment(oAppointment);
				} else {
					this._handleGroupAppointments(oEvent);
				}
			},

			handleOkButton: function (oEvent) {
				var oFrag =  sap.ui.core.Fragment,
					oStartValue = oFrag.byId("myPopoverFrag", "startDate").getDateValue(),
					oEndValue = oFrag.byId("myPopoverFrag", "endDate").getDateValue(),
					sInfoValue = oFrag.byId("myPopoverFrag", "moreInfo").getValue(),
					sAppointmentPath = this._oDetailsPopover.getBindingContext().sPath;

				this._oDetailsPopover.getModel().setProperty(sAppointmentPath + "/start", oStartValue);
				this._oDetailsPopover.getModel().setProperty(sAppointmentPath + "/end", oEndValue);
				this._oDetailsPopover.getModel().setProperty(sAppointmentPath + "/info", sInfoValue);
				this._oDetailsPopover.close();
			},

			handleCancelButton: function (oEvent) {
				this._oDetailsPopover.close();
			},
			onReturn: function(){
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("homepage");
			},

			handleAppointmentCreate: function (oEvent) {
				var oFrag =  sap.ui.core.Fragment,
					oDateTimePickerStart,
					oDateTimePickerEnd,
					oBeginButton;

				this._createDialog();

				oFrag.byId("myFrag", "selectPerson").setSelectedItem(oFrag.byId("myFrag", "selectPerson").getItems()[0]);

				oDateTimePickerStart = oFrag.byId("myFrag", "startDate");
				oDateTimePickerEnd =  oFrag.byId("myFrag", "endDate");
				oBeginButton = this.oNewAppointmentDialog.getBeginButton();

				oDateTimePickerStart.setValue("");
				oDateTimePickerEnd.setValue("");
				oDateTimePickerStart.setValueState("None");
				oDateTimePickerEnd.setValueState("None");

				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oBeginButton);
				this.oNewAppointmentDialog.open();
			},

			handleAppointmentAddWithContext: function (oEvent) {
				var oFrag =  sap.ui.core.Fragment,
					currentRow,
					sPersonName,
					oSelect,
					oSelectedItem,
					oSelectedIntervalStart,
					oStartDate,
					oSelectedIntervalEnd,
					oEndDate,
					oDateTimePickerStart,
					oDateTimePickerEnd,
					oBeginButton;

				this._createDialog();

				currentRow = oEvent.getParameter("row");
				sPersonName = currentRow.getTitle();
				oSelect = this.oNewAppointmentDialog.getContent()[0].getContent()[1];
				oSelectedItem = oSelect.getItems().filter(function(oItem) { return oItem.getText() === sPersonName; })[0];
				oSelect.setSelectedItem(oSelectedItem);

				oSelectedIntervalStart = oEvent.getParameter("startDate");
				oStartDate = oFrag.byId("myFrag", "startDate");
				oStartDate.setDateValue(oSelectedIntervalStart);

				oSelectedIntervalEnd = oEvent.getParameter("endDate");
				oEndDate = oFrag.byId("myFrag", "endDate");
				oEndDate.setDateValue(oSelectedIntervalEnd);

				oDateTimePickerStart = oFrag.byId("myFrag", "startDate");
				oDateTimePickerEnd =  oFrag.byId("myFrag", "endDate");
				oBeginButton = this.oNewAppointmentDialog.getBeginButton();

				oDateTimePickerStart.setValueState("None");
				oDateTimePickerEnd.setValueState("None");

				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oBeginButton);
				this.oNewAppointmentDialog.open();
			},

			_validateDateTimePicker: function (oDateTimePickerStart, oDateTimePickerEnd) {
				var oStartDate = oDateTimePickerStart.getDateValue(),
					oEndDate = oDateTimePickerEnd.getDateValue(),
					sValueStateText = "Start date should be before End date";

				if (oStartDate && oEndDate && oEndDate.getTime() <= oStartDate.getTime()) {
					oDateTimePickerStart.setValueState("Error");
					oDateTimePickerEnd.setValueState("Error");
					oDateTimePickerStart.setValueStateText(sValueStateText);
					oDateTimePickerEnd.setValueStateText(sValueStateText);
				} else {
					oDateTimePickerStart.setValueState("None");
					oDateTimePickerEnd.setValueState("None");
				}
			},

			updateButtonEnabledState: function (oDateTimePickerStart, oDateTimePickerEnd, oButton) {
				var bEnabled = oDateTimePickerStart.getValueState() !== "Error"
					&& oDateTimePickerStart.getValue() !== ""
					&& oDateTimePickerEnd.getValue() !== ""
					&& oDateTimePickerEnd.getValueState() !== "Error";

				oButton.setEnabled(bEnabled );
			},

			handleDetailsChange: function (oEvent) {
				var oFrag =  sap.ui.core.Fragment,
					oDTPStart = oFrag.byId("myPopoverFrag", "startDate"),
					oDTPEnd = oFrag.byId("myPopoverFrag", "endDate"),
					oOKButton = oFrag.byId("myPopoverFrag", "OKButton");

				if (oEvent.getParameter("valid")) {
					this._validateDateTimePicker(oDTPStart, oDTPEnd);
				} else {
					oEvent.getSource().setValueState("Error");
				}

				this.updateButtonEnabledState(oDTPStart, oDTPEnd, oOKButton);
			},

			handleCreateChange: function (oEvent) {
				var oFrag =  sap.ui.core.Fragment,
					oDateTimePickerStart = oFrag.byId("myFrag", "startDate"),
					oDateTimePickerEnd = oFrag.byId("myFrag", "endDate"),
					oBeginButton = this.oNewAppointmentDialog.getBeginButton();

				if (oEvent.getParameter("valid")) {
					this._validateDateTimePicker(oDateTimePickerStart, oDateTimePickerEnd);
				} else {
					oEvent.getSource().setValueState("Error");
				}

				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oBeginButton);
			},

			_createDialog: function () {
				var oFrag =  sap.ui.core.Fragment,
					that = this,
					oStartDate,
					oEndDate,
					sTitle,
					sInfoResponse,
					oNewAppointment,
					oModel,
					sPath,
					oPersonAppointments;

				if (!that.oNewAppointmentDialog) {

					that.oNewAppointmentDialog = new Dialog({
						title: 'Add a new schedule',
						content: [
							sap.ui.xmlfragment("myFrag", "swimmer_ui.swimmer_ui.view.Create", this)
						],
						beginButton: new Button({
							text: 'Create',
							enabled: false,
							press: function () {
								oStartDate = oFrag.byId("myFrag", "startDate").getDateValue();
								oEndDate = oFrag.byId("myFrag", "endDate").getDateValue();
								sTitle = oFrag.byId("myFrag", "inputTitle").getValue();
								sInfoResponse = oFrag.byId("myFrag", "moreInfo").getValue();

								if (oFrag.byId("myFrag", "startDate").getValueState() !== "Error"
									&& oFrag.byId("myFrag", "endDate").getValueState() !== "Error") {

									oNewAppointment = {
										start: oStartDate,
										end: oEndDate,
										title: sTitle,
										info: sInfoResponse
									};
									oModel = that.getView().getModel();
									sPath = "/people/" + oFrag.byId("myFrag", "selectPerson").getSelectedIndex() + "/appointments";
									oPersonAppointments = oModel.getProperty(sPath);

									oPersonAppointments.push(oNewAppointment);

									oModel.setProperty(sPath, oPersonAppointments);
									that.oNewAppointmentDialog.close();
								}
							}
						}),
						endButton: new Button({
							text: 'Close',
							press: function () {
								that.oNewAppointmentDialog.close();
							}
						})
					});

					that.oNewAppointmentDialog.addStyleClass("sapUiContentPadding");
					this.getView().addDependent(that.oNewAppointmentDialog);

				}
			},

			_handleSingleAppointment: function (oAppointment) {
				var oFrag =  sap.ui.core.Fragment,
					oAppBC,
					oDateTimePickerStart,
					oDateTimePickerEnd,
					oInfoInput,
					oOKButton;

				if (!this._oDetailsPopover) {
					this._oDetailsPopover = sap.ui.xmlfragment("myPopoverFrag", "swimmer_ui.swimmer_ui.view.Details", this);
					this.getView().addDependent(this._oDetailsPopover);
				}

				// the binding context is needed, because later when the OK button is clicked, the information must be updated
				oAppBC = oAppointment.getBindingContext();

				this._oDetailsPopover.setBindingContext(oAppBC);

				oDateTimePickerStart = oFrag.byId("myPopoverFrag", "startDate");
				oDateTimePickerEnd = oFrag.byId("myPopoverFrag", "endDate");
				oInfoInput = oFrag.byId("myPopoverFrag", "moreInfo");
				oOKButton = oFrag.byId("myPopoverFrag", "OKButton");

				oDateTimePickerStart.setDateValue(oAppointment.getStartDate());
				oDateTimePickerEnd.setDateValue(oAppointment.getEndDate());
				oInfoInput.setValue(oAppointment.getText());

				oDateTimePickerStart.setValueState("None");
				oDateTimePickerEnd.setValueState("None");

				this.updateButtonEnabledState(oDateTimePickerStart, oDateTimePickerEnd, oOKButton);
				this._oDetailsPopover.openBy(oAppointment);
			},

			_handleGroupAppointments: function (oEvent) {
				var aAppointments,
					sGroupAppointmentType,
					sGroupPopoverValue,
					sGroupAppDomRefId,
					bTypeDiffer;

				aAppointments = oEvent.getParameter("appointments");
				sGroupAppointmentType = aAppointments[0].getType();
				sGroupAppDomRefId = oEvent.getParameter("domRefId");
				bTypeDiffer = aAppointments.some(function (oAppointment) {
					return sGroupAppointmentType !== oAppointment.getType();
				});

				if (bTypeDiffer) {
					sGroupPopoverValue = aAppointments.length + " Appointments of different types selected";
				} else {
					sGroupPopoverValue = aAppointments.length + " Appointments of the same " + sGroupAppointmentType + " selected";
				}

				if (!this._oGroupPopover) {
					this._oGroupPopover = new Popover({
						title: "Group Appointments",
						content: new Label({
							text: sGroupPopoverValue
						})
					});
				} else {
					this._oGroupPopover.getContent()[0].setText(sGroupPopoverValue);
				}
				this._oGroupPopover.addStyleClass("sapUiPopupWithPadding");
				this._oGroupPopover.openBy(document.getElementById(sGroupAppDomRefId));
			}

		});

		return PageController;

	});
