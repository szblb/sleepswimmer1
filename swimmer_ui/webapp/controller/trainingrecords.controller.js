sap.ui.define([
	'sap/m/MessageBox',
	'sap/ui/core/Fragment',
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	'sap/m/MessageToast'],
 function (MessageBox, Fragment, Controller, JSONModel, MessageToast) {
	"use strict";

	return Controller.extend("swimmer_ui.swimmer_ui.controller.trainingrecords", {
oFormatYyyymmdd: null,
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.trainingrecords
		 */
	onInit: function () {
			var oData = {
				checkBox1Text : "CheckBox",
				checkBox2Text : "CheckBox - focused"
			};
			var oModel = new JSONModel(oData);
			var oView = this.getView();
			oView.setModel(oModel);
			
			this.oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({pattern: "yyyy-MM-dd", calendarType: sap.ui.core.CalendarType.Gregorian});
			
			oView.byId("DTP1").setValueFormat("yyyy-MM-dd");
		},
		onNavButtonPressed: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepage");
		},
		onSave: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var oView = this.getView();
			var sDay = oView.byId("DTP1").getDateValue();
			var month;
			var day;
			if(sDay.getMonth() < 9) {
				month = "0" + (sDay.getMonth() + 1);
			} else {
				month = sDay.getMonth() + 1;
			}
			if(sDay.getDate() <= 9) {
				day = "0" + sDay.getDate();
			} else {
				day = sDay.getDate();
			}
			
			var surveyDay = sDay.getFullYear() + "-" + month + "-" + day;
			var scheduleName = oView.byId("schedulename").getSelectedItem().getKey();
			var swimmingDistance = oView.byId("swimmingdistance").getValue();
			var trainingIntensity = oView.byId("trainingintensity").getSelectedItem().getKey();
			var duration = oView.byId("duration").getValue();
			var comments = oView.byId("comments").getValue();
			
			var response = jQuery.ajax({
				url: "/swimmer_java_api/setCoachDailySurvey/?surveyDay=" + surveyDay + "&scheduleName=" + scheduleName 
					 + "&swimmingDistance=" + swimmingDistance + "&trainingIntensity=" + trainingIntensity
					  + "&duration=" + duration+ "&comments=" + comments,
				method: "GET",
				async: false
			}).responseText;
			
			if(response === "NOK") {
				var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				MessageBox.success(
					"You already completed a survey for that day!",
					{
						styleClass: bCompact ? "sapUiSizeCompact" : ""
					}
				);
			} else {
				oRouter.navTo("homepage");
			}
		},
		
		handleCalendarSelect: function(oEvent) {
			var oCalendar = oEvent.getSource();
			this._updateText(oCalendar);
		},

		_updateText: function(oCalendar) {
			var oText = this.byId("selectedDate");
			var aSelectedDates = oCalendar.getSelectedDates();
			var oDate;
			oDate = aSelectedDates[0].getStartDate();
			oText.setText(this.oFormatYyyymmdd.format(oDate));
		},
		
		onExit: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepage");
		}
	});

});