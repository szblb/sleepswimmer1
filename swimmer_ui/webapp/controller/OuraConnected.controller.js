sap.ui.define([
	"swimmer_ui/swimmer_ui/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("swimmer_ui.swimmer_ui.controller.OuraConnected", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.OuraConnected
		 */
		onInit: function () {
			this.getRouter().getRoute("ouraConnected").attachPatternMatched(this._onRouteMatched,this);
		},
		
		_onRouteMatched: function(oEvent){
			var code = oEvent.getParameter("arguments").code;
			var state = oEvent.getParameter("arguments").state;
			console.log("After successfully connected to Oura, got code=" + code + " and state=" + state);
			if (this.checkIfUserAllowedOura() === true) {
				this.getView().byId("ouraConnectedTitleId").setText("You already allowed us to access your Oura API data!");
			} else {
				//TODO give tokens to backend
				
				jQuery.ajax({
					url: "/swimmer_java_api/callback?code=" + code + "&state=" + state,
					method: "GET",
					dataType: "json",
					async: false
				});
				
				this.getView().byId("ouraConnectedTitleId").setText("Thank you for allowing us to access your Oura API data!");
			}
		}

	});

});