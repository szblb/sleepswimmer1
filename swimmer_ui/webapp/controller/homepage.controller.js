sap.ui.define([
	"swimmer_ui/swimmer_ui/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("swimmer_ui.swimmer_ui.controller.homepage", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.homepage
		 */
		onInit: function () {
			sap.ui.core.UIComponent.getRouterFor(this).getRoute("homepage").attachPatternMatched(this._onRouteMatched,this);
		},
		
		_onRouteMatched: function() {
			var oView = this.getView();
			this.redirectToCorrectPage();
			
			var response = jQuery.ajax({
				url: "/swimmer_java_api/getUserInfo",
				method: "GET",
				async: false
			}).responseText;
			var json = JSON.parse(response);
			var userName = json["name"];
			oView.byId("title").setText("Welcome, " + userName + "!");
			
			
			var oModel = new sap.ui.model.json.JSONModel({
		      	"TileCollection" : [
		{
			"icon" : "sap-icon://account",
			"number" : "21",
			"title" : "Swimmers Profile Data",
			"infoState" : "Error"
		},
		{
			"icon" : "sap-icon://physical-activity",
			"number" : "5",
			"title" : "Training Schedule and Calendar",
			"infoState" : "Error"
		},
		{
			"icon" : "sap-icon://business-by-design",
			"number" : "15",
			"title" : "Daily Training Records",
			"infoState" : "Error"
		}
		],
			"TileeCollection" : [
			{
			"icon" : "sap-icon://multiple-bar-chart",
			"title" : "Team Dashboard",
			"infoState" : "Error"
		},
			{
			"icon" : "sap-icon://electrocardiogram",
			"title" : "Detailed OURA Analysis",
			"infoState" : "Error"
		}
		
	]
		    });
		    oModel.setDefaultBindingMode("TwoWay");
		    this.getView().setModel(oModel);
		},
		
		handleTilePress: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			if (oEvent.getSource().getHeader() === 'Swimmers Profile Data')
			{
				oRouter.navTo("view1");
			}
			else if(oEvent.getSource().getHeader() === 'Training Schedule and Calendar')
			{
				oRouter.navTo("TrainingGrid");
			}
			else if(oEvent.getSource().getHeader() === 'Daily Training Records')
			{
				oRouter.navTo("trainingrecords");
			}
			else if(oEvent.getSource().getHeader() === 'Team Dashboard')
			{
				oRouter.navTo("teamdashboard");
			}
			
		}
	});

});