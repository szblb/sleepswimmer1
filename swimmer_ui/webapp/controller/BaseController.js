sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/ui/core/routing/History",
		"sap/m/MessageToast"
	], function (Controller, History, MessageToast) {
		"use strict";

		return Controller.extend("swimmer_ui.swimmer_ui.controller.BaseController", {
			
			getRouter : function () {
				return sap.ui.core.UIComponent.getRouterFor(this);
			},
			
			onPressUserIcon: function(oEvent) {
				var oButton = oEvent.getSource();
	
				// create action sheet only once
				if (!this._actionSheet) {
					this._actionSheet = sap.ui.xmlfragment("swimmer_ui.swimmer_ui.fragments.ActionSheet",	this);
					this.getView().addDependent(this._actionSheet);
				}

				this._actionSheet.openBy(oButton);
			},
			
			onPressLogout: function() {
				sap.m.URLHelper.redirect("/do/logout");
			},
			
			onPressAccount:function() {
				if (!this._oAccountDialog) {
					this._oAccountDialog = sap.ui.xmlfragment("swimmer_ui.swimmer_ui.fragments.AccountTab", this);
					this.getView().addDependent(this._oAccountDialog);
				}
				// toggle compact style
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oAccountDialog);
				this._oAccountDialog.open();
			},
			
			onCloseAccountDialog: function() {
				this._oAccountDialog.close();
			},
			
			onPressSettings:function() {
				if (!this._oDialog) {
					this._oDialog = sap.ui.xmlfragment("swimmer_ui.swimmer_ui.fragments.SettingsTab", this);
					this.getView().addDependent(this._oDialog);
				}
				// toggle compact style
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
				this._oDialog.open();
			},
			
			onConfirmSettingsDialog: function (oEvent) {
				var oHelloModel = this.getModel("helloPanelViewModel");
				var newLanguageSet = this._oDialog.getContent()[1].getSelectedKey();
				var oldLanguageSet = oHelloModel.getProperty("/language");
				
				if (oldLanguageSet !== newLanguageSet) {
					oHelloModel.setProperty("/language", newLanguageSet);
					sap.ui.getCore().getConfiguration().setLanguage(newLanguageSet);
				}
				
				this._oDialog.close();
			},
			
			onCancelSettingsDialog: function (oEvent) {
				this._oDialog.close();
			},
			
			onPressOuraAllowButton: function() {
				console.log("You pressed allow, a new window should have opened to let you allow Oura.");
				sap.m.URLHelper.redirect("/swimmer_java_api/ouraLogin",0);
			},
			
			onOpenOuraAllowDialog: function() {
				if (!this._oOuraAllowDialog) {
					this._oOuraAllowDialog = sap.ui.xmlfragment("swimmer_ui.swimmer_ui.fragment.OuraAllow", this);
					this.getView().addDependent(this._oOuraAllowDialog);
				}
				// toggle compact style
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oOuraAllowDialog);
				this._oOuraAllowDialog.open();
			},
			
			onCloseOuraAllowDialog: function(oEvent) {
				this._oOuraAllowDialog.close();
			},
			
			getModel : function (sName) {
				return this.getView().getModel(sName);
			},

			setModel : function (oModel, sName) {
				return this.getView().setModel(oModel, sName);
			},

			getResourceBundle : function () {
				return this.getOwnerComponent().getModel("i18n").getResourceBundle();
			},
			
			onNavBackOrTo: function (route) {
				var oHistory = History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();
				
				if (sPreviousHash !== undefined) {
					window.history.go(-1);
				} else {
					//TODO: change this to see the correct slide of the screen
					//currently it is sliding the wrong way
					this.getRouter().navTo(route, {}, true /*no history*/);
				}
			},

        	showMessageToast: function (msg) {
        	    MessageToast.show(msg, {
        	        duration: 3000,                    // default
        	        collision: "fit fit",            // default
        	        onClose: null,                   // default
        	        autoClose: true,                 // default
        	        animationTimingFunction: "ease", // default
        	        animationDuration: 1000,         // default
        	        closeOnBrowserNavigation: true   // default
        	    });
        	},
		
			checkIfUserAllowedOura: function() {
				var response = jQuery.ajax({
					url: "/swimmer_java_api/checkIfUserAllowedOura/",
					method: "GET",
					async: false
				}).responseText;
				if(response === "OK") {
					return true;
				} else {
					return false;
				}
			},
			
			getUserSwimmerOrCoach: function() {
				var response = jQuery.ajax({
					url: "/swimmer_java_api/getUserSwimmerOrCoach/",
					method: "GET",
					async: false
				}).responseText;
				
				return response;
			},
			
			redirectBasedOnType: function(userType) {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				if(userType === "Swimmer+Coach") {
					//redirect to any of the views, the coach view is the more important for now
					oRouter.navTo("homepage");
				} else if(userType === "Coach") {
					oRouter.navTo("homepage");
				} else if(userType === "Swimmer") {
					oRouter.navTo("homepagesw");
				} else if(userType === "SwimmerIncompleteProfile"){
					oRouter.navTo("editprofilesw");
				} else if(userType === "SwimmerIncompleteProfile+Coach"){
					oRouter.navTo("editprofilesw");
				} else {
					//userType === "Nothing"
				}
			},
			
			redirectToCorrectPage: function() {
				var response = jQuery.ajax({
					url: "/swimmer_java_api/getUserSwimmerOrCoach/",
					method: "GET",
					async: false
				}).responseText;
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				if(response === "Swimmer+Coach") {
					//redirect to any of the views, the coach view is the more important for now
					oRouter.navTo("homepage");
				} else if(response === "Coach") {
					oRouter.navTo("homepage");
				} else if(response === "Swimmer") {
					oRouter.navTo("homepagesw");
				} else if(response === "SwimmerIncompleteProfile"){
					oRouter.navTo("editprofilesw");
				} else if(response === "SwimmerIncompleteProfile+Coach"){
					oRouter.navTo("editprofilesw");
				} else {
					//response === "Nothing"
					oRouter.navTo("WelcomePage");
				}
			}
		});

	}
);