sap.ui.define([
	'sap/m/MessageBox',
		'sap/ui/core/Fragment',
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel',
		'sap/m/MessageToast'], 
function ( MessageBox, Fragment, Controller, JSONModel, MessageToast) {
	"use strict";

	return Controller.extend("swimmer_ui.swimmer_ui.controller.schedule", {
	oFormatYyyymmdd: null,
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.schedule
		 */
		onInit: function () {
			var oModel = new sap.ui.model.json.JSONModel({
		      SwimmersCollection : [
			    {"Name": "Claire DUPONT"},
		        {"Name": "Marie-Anne TRUFFAUT"},
		        {"Name": "Adrien LEBEL"},
		        {"Name": "Téophane BIDDAUT"},
		        {"Name": "Amélie PIFFAULT"},
		        {"Name": "Tancrède HAYER"},
		        {"Name": "Quentin BAJOT"}
		         ]
		    });
			/*var oModel = new JSONModel(oData);*/
			var oView = this.getView();
			oView.setModel(oModel);
			this.oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({pattern: "yyyy-MM-dd", calendarType: sap.ui.core.CalendarType.Gregorian});
		},
	handleSaveMessageBoxPress: function(oEvent) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox.success(
				"Your profile has been saved successfully!",
				{
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		},
		onExit: function(){
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oRouter.navTo("homepage");
	},
		handleCalendarSelect: function(oEvent) {
			var oCalendar = oEvent.getSource();
			this._updateText(oCalendar);
		},

		_updateText: function(oCalendar) {
			var oText = this.byId("selectedDate");
			var aSelectedDates = oCalendar.getSelectedDates();
			var oDate;
			oDate = aSelectedDates[0].getStartDate();
			oText.setText(this.oFormatYyyymmdd.format(oDate));
		}


		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf swimmer_ui.swimmer_ui.view.schedule
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf swimmer_ui.swimmer_ui.view.schedule
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf swimmer_ui.swimmer_ui.view.schedule
		 */
		//	onExit: function() {
		//
		//	}

	});

});