sap.ui.define([
	"swimmer_ui/swimmer_ui/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("swimmer_ui.swimmer_ui.controller.WelcomePage", {

		onInit: function () {
			var userType = this.getUserSwimmerOrCoach();
			this.redirectBasedOnType(userType);
		},
		
		setUserSwimmer: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var userType = jQuery.ajax({
				url: "/swimmer_java_api/setUserSwimmer/",
				method: "GET",
				async: false
			}).responseText;
			if(userType === "SwimmerIncompleteProfile") {
				oRouter.navTo("editprofilesw");
			}
		},
		
		setUserCoach: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var userType = jQuery.ajax({
				url: "/swimmer_java_api/setUserCoach/",
				method: "GET",
				async: false
			}).responseText;
			if(userType === "Coach") {
				oRouter.navTo("homepage");
			}
		}
	});

});