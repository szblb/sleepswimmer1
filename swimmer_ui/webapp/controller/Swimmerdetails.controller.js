sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("swimmer_ui.swimmer_ui.controller.Swimmerdetails", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.Swimmerdetails
		 */
		onInit: function () {
			var oRouter=sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("Swimmerdetails").attachPatternMatched(this._onRouteMatched,this);
		},
		
		_onRouteMatched: function(oEvent){
			var name=oEvent.getParameter("arguments").name;
			var age=oEvent.getParameter("arguments").age;
			var gender=oEvent.getParameter("arguments").gender;
			var height=oEvent.getParameter("arguments").height;
			var weight=oEvent.getParameter("arguments").weight;
			var stroke_speciality=oEvent.getParameter("arguments").stroke_speciality;
			var status=oEvent.getParameter("arguments").status;
			var training_program=oEvent.getParameter("arguments").training_program;
			
			var oModel = new sap.ui.model.json.JSONModel({
		      DetailsList : [oEvent.getParameter("arguments")]
			});
			console.log(oModel);
			 oModel.setDefaultBindingMode("TwoWay");
		    this.getView().setModel(oModel);
		}
	
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf swimmer_ui.swimmer_ui.view.Swimmerdetails
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf swimmer_ui.swimmer_ui.view.Swimmerdetails
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf swimmer_ui.swimmer_ui.view.Swimmerdetails
		 */
		//	onExit: function() {
		//
		//	}

	});

});