sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("swimmer_ui.swimmer_ui.controller.displayA", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf swimmer_ui.swimmer_ui.view.displayA
		 */
		onInit: function () {
			/*
var oModel = new sap.ui.model.json.JSONModel({
		      SwimmersList : [
			    {
		          "Name": "Claire DUPONT",
		          "Gender": "F",
		          "Age": "14",     
		          "Weight": "50",
		          "Height": "161m", 
		          "Stroke_Speciality": "Backstroke",
		          "Status": "Available"
		          
		        },
		         {
		          "Name": "Marie-Anne TRUFFAUT",
		          "Gender": "F",
		          "Age": "18",     
		          "Weight": "50",
		          "Height": "170m", 
		          "Stroke_Speciality": "Breaststroke",
		          "Status": "Available"
		          
		        },
		        {
		          "Name": "Adrien LEBEL",
		          "Gender": "M",
		          "Age": "11",     
		          "Weight": "85",
		          "Height": "195m", 
		          "Stroke_Speciality": "Freestyle",
		          "Status":"Available"
		        },
		        {
		          "Name": "Téophane BIDDAUT",
		          "Gender": "M",
		          "Age": "17",     
		          "Weight": "70",
		          "Height": "185m", 
		          "Stroke_Speciality": "Butterfly",
		          "Status":"Available"
		        },
		         {
		          "Name": "Amélie Piffaut",
		          "Gender": "F",
		          "Age": "23",     
		          "Weight": "65",
		          "Height": "175m", 
		          "Stroke_Speciality": "Freestyle",
		          "Status":"NA"
		        },
		         {
		          "Name": "Tancrède HAYER",
		          "Gender": "M",
		          "Age": "20",     
		          "Weight": "80",
		          "Height": "185m", 
		          "Stroke_Speciality": "Breaststroke",
		          "Status":"NA"
		        },
		         {
		          "Name": "Quentin BAJOT",
		          "Gender": "M",
		          "Age": "16",     
		          "Weight": "75",
		          "Height": "185m", 
		          "Stroke_Speciality": "Butterfly",
		          "Status":"Available"
		        }
		        
		      ]
		    });
		    oModel.setDefaultBindingMode("TwoWay");
		    this.getView().setModel(oModel);
		    */
		},
		onReturn: function(){
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oRouter.navTo("homepage");
	}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf swimmer_ui.swimmer_ui.view.displayA
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf swimmer_ui.swimmer_ui.view.displayA
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf swimmer_ui.swimmer_ui.view.displayA
		 */
		//	onExit: function() {
		//
		//	}

	});

});