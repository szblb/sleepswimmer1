sap.ui.define([
	"swimmer_ui/swimmer_ui/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("swimmer_ui.swimmer_ui.controller.homepagesw", {

		onInit: function () {
			sap.ui.core.UIComponent.getRouterFor(this).getRoute("homepagesw").attachPatternMatched(this._onRouteMatched,this);
		},
		
		_onRouteMatched: function() {
			var oView = this.getView();
			this.redirectToCorrectPage();
			
			var response = jQuery.ajax({
				url: "/swimmer_java_api/getUserInfo",
				method: "GET",
				async: false
			}).responseText;
			var json = JSON.parse(response);
			var userName = json["name"];
			oView.byId("title").setText("Welcome, " + userName + "!");
			
			var oModel = new sap.ui.model.json.JSONModel({
		      	"TileCollection" : [
		{
			"icon" : "sap-icon://account",
			"title" : "Your Profile",
			"infoState" : "Error"
		},
       {
			"icon" : "sap-icon://survey",
			"title" : "Daily Survey",
			"infoState" : "Error"
		}
		],
			"TileeCollection" : [
		{
			"icon" : "sap-icon://multiple-bar-chart",
			"title" : "Your Dashboard",
			"infoState" : "Error"
		},
		{
			"icon" : "sap-icon://multiple-line-chart",
			"title" : "Detailed OURA Analytics",
			"infoState" : "Error"
		}
	]
		    });
		    oModel.setDefaultBindingMode("TwoWay");
		    this.getView().setModel(oModel);	
		},
		
		handleTilePress: function(oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			if (oEvent.getSource().getHeader() === 'Your Profile')
			{
				oRouter.navTo("editprofilesw");
			}
			else if(oEvent.getSource().getHeader() === 'Daily Survey')
			{
				oRouter.navTo("yourdailysurvey1");
			} 
			else if(oEvent.getSource().getHeader() === 'Your Dashboard')
			{
				oRouter.navTo("yourdashboard");
			}
			
		}

	});

});