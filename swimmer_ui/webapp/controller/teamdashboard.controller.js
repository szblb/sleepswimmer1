sap.ui.define([
	"swimmer_ui/swimmer_ui/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("swimmer_ui.swimmer_ui.controller.teamdashboard", {

		onInit: function () {
			sap.ui.core.UIComponent.getRouterFor(this).getRoute("teamdashboard").attachPatternMatched(this._onRouteMatched,this);
		},
		
		_onRouteMatched: function() {
			var oView = this.getView();
			
		},
		onNavButtonPressed: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepage");
		}
	});

});