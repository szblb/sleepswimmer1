sap.ui.define([
	'sap/m/MessageBox',
	'sap/ui/core/Fragment',
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	'sap/m/MessageToast'],
function (MessageBox, Fragment, Controller, JSONModel, MessageToast) {
	"use strict";
	return Controller.extend("swimmer_ui.swimmer_ui.controller.yourdailysurvey1", {
		oFormatYyyymmdd: null,

		onInit: function () {
			var oData = {
				checkBox1Text : "CheckBox",
				checkBox2Text : "CheckBox - focused"
			};
			var oModel = new JSONModel(oData);
			var oView = this.getView();
			oView.setModel(oModel);
			
			this.oFormatYyyymmdd = sap.ui.core.format.DateFormat.getInstance({pattern: "yyyy-MM-dd", calendarType: sap.ui.core.CalendarType.Gregorian});

		},
		onNavButtonPressed: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepagesw");
		},
		
		onSave: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var oView = this.getView();
			var sDay = new Date(oView.byId("calendar").getSelectedDates()[0].getStartDate());
			var month;
			var day;
			if(sDay.getMonth() < 9) {
				month = "0" + (sDay.getMonth() + 1);
			} else {
				month = sDay.getMonth() + 1;
			}
			if(sDay.getDate() <= 9) {
				day = "0" + sDay.getDate();
			} else {
				day = sDay.getDate();
			}
			var surveyDay = sDay.getFullYear() + "-" + month + "-" + day;
			var specialityBestTime = oView.byId("sBestTime").getValue();
			var bestDistancePerObjective = 100;//oView.byId("bestDistancePerObjective").getValue();
			var specialityNumberOfStrokes = oView.byId("sNumberOfStrokes").getValue();
			var personalSwimmingDistance = oView.byId("personalSwimmingDistance").getValue();
			
			var response = jQuery.ajax({
				url: "/swimmer_java_api/setUserDailySurvey1/?surveyDay=" + surveyDay + "&specialityBestTime=" + specialityBestTime 
					 + "&bestDistancePerObjective=" + bestDistancePerObjective + "&specialityNumberOfStrokes=" + specialityNumberOfStrokes
					  + "&personalSwimmingDistance=" + personalSwimmingDistance,
				method: "GET",
				async: false
			}).responseText;
			
			if(response === "NOK") {
				var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				MessageBox.success(
					"You already completed a survey for that day!",
					{
						styleClass: bCompact ? "sapUiSizeCompact" : ""
					}
				);
			} else {
				oRouter.navTo("yourdailysurvey2", {
                	surveyDay: surveyDay
            	});
			}
		},
		
		onExit: function(){
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("homepagesw");
		},
		
		handleCalendarSelect: function(oEvent) {
			var oCalendar = oEvent.getSource();
			this._updateText(oCalendar);
		},

		_updateText: function(oCalendar) {
			var oText = this.byId("selectedDate");
			var aSelectedDates = oCalendar.getSelectedDates();
			var oDate;
			oDate = aSelectedDates[0].getStartDate();
			oText.setText(this.oFormatYyyymmdd.format(oDate));
		}

	});

});