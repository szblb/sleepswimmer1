package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::yourdailysurvey1\"")
public class YourDailySurvey1 {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"survey_day\"")
    private LocalDate surveyDay;
    
    @Column(name="\"speciality_best_time\"")
    private double specialityBestTime;
    
    @Column(name="\"best_distance_per_objective\"")
    private double bestDistancePerObjective;
    
    @Column(name="\"speciality_number_of_strokes\"")
    private double specialityNumberOfStrokes;
    
    @Column(name="\"personal_swimming_distance\"")
    private double personalSwimmingDistance;
	
    public YourDailySurvey1() {
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
    public String getId() {
    	return id;
    }
    
    public void setUserId(String userId) {
    	this.userId = userId;
    }
    
    public String getUserId() {
    	return userId;
    }
    
    public void setSurveyDay(LocalDate surveyDay) {
    	this.surveyDay = surveyDay;
    }
    
    public LocalDate getSurveyDay() {
    	return surveyDay;
    }
    
    public void setSpecialityBestTime(double specialityBestTime) {
    	this.specialityBestTime = specialityBestTime;
    }
    
    public double getSpecialityBestTime() {
    	return specialityBestTime;
    }
    
    public void setBestDistancePerObjective(double bestDistancePerObjective) {
    	this.bestDistancePerObjective = bestDistancePerObjective;
    }
    
    public double getBestDistancePerObjective() {
    	return bestDistancePerObjective;
    }
    
    public void setSpecialityNumberOfStrokes(double specialityNumberOfStrokes) {
    	this.specialityNumberOfStrokes = specialityNumberOfStrokes;
    }
    
    public double getSpecialityNumberOfStrokes() {
    	return specialityNumberOfStrokes;
    }
    
    public void setPersonalSwimmingDistance(double personalSwimmingDistance) {
    	this.personalSwimmingDistance = personalSwimmingDistance;
    }
    
    public double getPersonalSwimmingDistance() {
    	return personalSwimmingDistance;
    }
}
