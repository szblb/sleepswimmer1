package com.company.sleepswimmer1.swimmer_java.repository;

import com.company.sleepswimmer1.swimmer_java.entity.Rmssd5Min;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Rmssd5MinRepository extends CrudRepository<Rmssd5Min, String> {
}
