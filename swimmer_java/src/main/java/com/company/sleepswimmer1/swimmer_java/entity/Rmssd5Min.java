package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::rmssd_5min\"")
public class Rmssd5Min {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"rmssd_5min_id\"")
    private String rmssd5MinId;
    
    @Column(name="\"rmssd_5min_value\"")
    private int rmssd5MinValue;
    
    @Column(name="\"rmssd_5min_start\"")
    private ZonedDateTime rmssd5MinStart;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getRmssd5MinId() {
        return rmssd5MinId;
    }

    public void setRmssd5MinId(String rmssd5MinId) {
        this.rmssd5MinId = rmssd5MinId;
    }
    
    public int getRmssd5MinValue() {
        return rmssd5MinValue;
    }

    public void setRmssd5MinValue(int rmssd5MinValue) {
        this.rmssd5MinValue = rmssd5MinValue;
    }
    
    public ZonedDateTime getRmssd5MinStart() {
        return rmssd5MinStart;
    }

    public void setRmssd5MinStart(ZonedDateTime rmssd5MinStart) {
        this.rmssd5MinStart = rmssd5MinStart;
    }
}
