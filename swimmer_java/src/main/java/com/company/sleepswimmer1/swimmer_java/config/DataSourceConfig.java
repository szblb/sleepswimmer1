package com.company.sleepswimmer1.swimmer_java.config;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Configuration Bean used for database url/username/password config.
 */
@Component
@Configuration
public class DataSourceConfig {
    public static String DATABASE_MANAGEMENT_SYSTEM = "sap";
    public static String DATABASE_TYPE = "hana";

    @Value("${mock.db.url}")
    private String mock_db_url;

    @Value("${mock.db.username}")
    private String mock_db_username;

    @Value("${mock.db.password}")
    private String mock_db_password;

    /**
     * @return DataSource containing database connection info.
     */
    @Bean
    public DataSource getDataSource() {

        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();

        List<String> connInfo = this.getDatabaseConnectionInfo(DATABASE_MANAGEMENT_SYSTEM, DATABASE_TYPE);
        //comment this ^ to work with custom database credentials defined in application.properties
        //List<String> connInfo = new ArrayList<>();
        if (!connInfo.isEmpty()) {
        	System.out.println("Configured Spring Data connection using VCAP_SERVICES.");
            dataSourceBuilder.url(connInfo.get(0));
            dataSourceBuilder.username(connInfo.get(1));
            dataSourceBuilder.password(connInfo.get(2));
            System.out.println("URL=" + connInfo.get(0));
            System.out.println("USERNAME=" + connInfo.get(1));
            System.out.println("PASSWORD=" + connInfo.get(2));
        } else {
        	System.out.println("Configured Spring Data connection using mock credentials.");
            dataSourceBuilder.url(mock_db_url);
            dataSourceBuilder.username(mock_db_username);
            dataSourceBuilder.password(mock_db_password);
        }

        return dataSourceBuilder.build();
    }

    /**
     * Method used to retrieve Database connection info from VCAP_SERVICES.
     *
     * @param dbms   the DataBase Management System
     * @param dbtype the DataBase Type
     * @return List containing 3 String elements in order: the database url, the username, the password
     */
    private List<String> getDatabaseConnectionInfo(String dbms, String dbtype) {
        List<String> dbinfo = new ArrayList<>();
        try {
            String vcapServices = System.getenv("VCAP_SERVICES");
            if (vcapServices == null) {
                return dbinfo;
            }
            JSONObject obj = new JSONObject(vcapServices);
            JSONArray arr = obj.getJSONArray(dbtype);
            JSONObject credentials = arr.getJSONObject(0).getJSONObject("credentials");

            String DB_USERNAME = credentials.getString("user");
            String DB_PASSWORD = credentials.getString("password");
            String DB_HOST = credentials.getString("host").split(",")[0];
            String DB_PORT = credentials.getString("port");
            String DB_SCHEMA = credentials.getString("schema");
            String DB_URL = "jdbc:" + dbms + "://" + DB_HOST + ":" + DB_PORT + "?encrypt=true&currentSchema=" + DB_SCHEMA;

            dbinfo.add(0, DB_URL);
            dbinfo.add(1, DB_USERNAME);
            dbinfo.add(2, DB_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbinfo;
    }
}