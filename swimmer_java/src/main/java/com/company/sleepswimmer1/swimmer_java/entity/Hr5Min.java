package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::hr_5min\"")
public class Hr5Min {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"hr_5min_id\"")
    private String hr5MinId;
    
    @Column(name="\"hr_5min_value\"")
    private int hr5MinValue;
    
    @Column(name="\"hr_5min_start\"")
    private ZonedDateTime hr5MinStart;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getHr5MinId() {
        return hr5MinId;
    }

    public void setHr5MinId(String hr5MinId) {
        this.hr5MinId = hr5MinId;
    }
    
    public int getHr5MinValue() {
        return hr5MinValue;
    }

    public void setHr5MinValue(int hr5MinValue) {
        this.hr5MinValue = hr5MinValue;
    }
    
    public ZonedDateTime getHr5MinStart() {
        return hr5MinStart;
    }

    public void setHr5MinStart(ZonedDateTime hr5MinStart) {
        this.hr5MinStart = hr5MinStart;
    }
}
