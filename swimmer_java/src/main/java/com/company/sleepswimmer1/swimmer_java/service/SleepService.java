package com.company.sleepswimmer1.swimmer_java.service;

import com.company.sleepswimmer1.swimmer_java.entity.Sleep;
import com.company.sleepswimmer1.swimmer_java.repository.SleepRepository;
import com.company.sleepswimmer1.swimmer_java.entity.Hypnogram5Min;
import com.company.sleepswimmer1.swimmer_java.repository.Hypnogram5MinRepository;
import com.company.sleepswimmer1.swimmer_java.entity.Hr5Min;
import com.company.sleepswimmer1.swimmer_java.repository.Hr5MinRepository;
import com.company.sleepswimmer1.swimmer_java.entity.Rmssd5Min;
import com.company.sleepswimmer1.swimmer_java.repository.Rmssd5MinRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class SleepService {
    @Autowired
    private SleepRepository sleepRepository;
	
	@Autowired
    private Hypnogram5MinRepository hypnogram5MinRepository;
    
    @Autowired
    private Hr5MinRepository hr5MinRepository;
    
    @Autowired
    private Rmssd5MinRepository rmssd5MinRepository;

    public void saveMultipleDaysSleepInfo(String stringSleepInfo, String userId) {
        try {
            JSONArray jsonSleepInfo = new JSONObject(stringSleepInfo).getJSONArray("sleep");

            final int numberOfItemsInJson = jsonSleepInfo.length();
            for (int i = 0; i < numberOfItemsInJson; i++){
                JSONObject day = jsonSleepInfo.getJSONObject(i);
                this.saveSingleDaySleepFromJson(day, userId);
                System.out.println("Successfully saved a new Sleep Info day!");
            }
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    public void saveSingleDaySleepFromJson(JSONObject day, String userId) throws JSONException {
        Sleep sleep = new Sleep();
        
		sleep.setUserId(userId);
		sleep.setSummaryDate(day.getString("summary_date"));
		sleep.setPeriodId(day.getInt("period_id"));
		sleep.setIsLongest(day.getInt("is_longest"));
		sleep.setTimezone(day.getInt("timezone"));
		sleep.setBedtimeStart(ZonedDateTime.parse(day.getString("bedtime_start")));
		sleep.setBedtimeEnd(ZonedDateTime.parse(day.getString("bedtime_end")));
		sleep.setScore(day.getInt("score"));
		sleep.setScoreTotal(day.getInt("score_total"));
		sleep.setScoreDisturbances(day.getInt("score_disturbances"));
		sleep.setScoreEfficiency(day.getInt("score_efficiency"));
		sleep.setScoreLatency(day.getInt("score_latency"));
		sleep.setScoreRem(day.getInt("score_rem"));
		sleep.setScoreDeep(day.getInt("score_deep"));
		sleep.setScoreAlignment(day.getInt("score_alignment"));
		sleep.setTotal(day.getInt("total"));
		sleep.setDuration(day.getInt("duration"));
		sleep.setAwake(day.getInt("awake"));
		sleep.setLight(day.getInt("light"));
		sleep.setRem(day.getInt("rem"));
		sleep.setDeep(day.getInt("deep"));
		sleep.setOnsetLatency(day.getInt("onset_latency"));
		sleep.setRestless(day.getInt("restless"));
		sleep.setEfficiency(day.getInt("efficiency"));
		sleep.setMidpointTime(day.getInt("midpoint_time"));
		sleep.setHrLowest(day.getInt("hr_lowest"));
		sleep.setHrAverage(day.getFloat("hr_average"));
		sleep.setBreathAverage(day.getFloat("breath_average"));
		sleep.setTemperatureDelta(day.getDouble("temperature_delta"));
		sleep.setHypnogram5MinId(UUID.randomUUID().toString());
		sleep.setHr5MinId(UUID.randomUUID().toString());
		sleep.setRmssd5MinId(UUID.randomUUID().toString());
  
        sleepRepository.save(sleep);
		
		//TODO continue this
		this.saveHypnogram5MinFromJson(day, sleep.getHypnogram5MinId(), sleep.getBedtimeStart(), userId);
		this.saveHr5MinFromJson(day, sleep.getHr5MinId(), sleep.getBedtimeStart(), userId);
		this.saveRmssd5MinFromJson(day, sleep.getRmssd5MinId(), sleep.getBedtimeStart(), userId);
    }
	
	private void saveHypnogram5MinFromJson(JSONObject day, String hypnogram5MinId, ZonedDateTime bedtimeStart, String userId) {
		int[] hypnogram5MinArray = this.stringSplitIntoIntArray(day.getString("hypnogram_5min"));
		// list to save it as a bulk
        List<Hypnogram5Min> listToSave = new ArrayList<>();
        Hypnogram5Min hypnogram5Min;
        for (int i = 0; i < hypnogram5MinArray.length; i++) {
            hypnogram5Min = new Hypnogram5Min();
            hypnogram5Min.setUserId(userId);
			hypnogram5Min.setHypnogram5MinId(hypnogram5MinId);
			hypnogram5Min.setHypnogram5MinValue(hypnogram5MinArray[i]);
			hypnogram5Min.setHypnogram5MinStart(bedtimeStart);
            //add the activity/minute to the list
            listToSave.add(hypnogram5Min);
            //go to the next 5 minutes
            bedtimeStart = bedtimeStart.plusMinutes(5);
        }
        hypnogram5MinRepository.saveAll(listToSave);
	}
	
	private void saveHr5MinFromJson(JSONObject day, String hr5MinId, ZonedDateTime bedtimeStart, String userId) {
		int[] hr5MinArray = this.jsonArrayToIntArray(day.getJSONArray("hr_5min"));
		// list to save it as a bulk
        List<Hr5Min> listToSave = new ArrayList<>();
        Hr5Min hr5Min;
        for (int i = 0; i < hr5MinArray.length; i++) {
            hr5Min = new Hr5Min();
            hr5Min.setUserId(userId);
			hr5Min.setHr5MinId(hr5MinId);
			hr5Min.setHr5MinValue(hr5MinArray[i]);
			hr5Min.setHr5MinStart(bedtimeStart);
            //add the activity/minute to the list
            listToSave.add(hr5Min);
            //go to the next 5 minutes
            bedtimeStart = bedtimeStart.plusMinutes(5);
        }
        hr5MinRepository.saveAll(listToSave);
	}
	
	private void saveRmssd5MinFromJson(JSONObject day, String rmssd5MinId, ZonedDateTime bedtimeStart, String userId) {
		int[] rmssd5MinArray = this.jsonArrayToIntArray(day.getJSONArray("rmssd_5min"));
		// list to save it as a bulk
        List<Rmssd5Min> listToSave = new ArrayList<>();
        Rmssd5Min rmssd5Min;
        for (int i = 0; i < rmssd5MinArray.length; i++) {
            rmssd5Min = new Rmssd5Min();
            rmssd5Min.setUserId(userId);
			rmssd5Min.setRmssd5MinId(rmssd5MinId);
			rmssd5Min.setRmssd5MinValue(rmssd5MinArray[i]);
			rmssd5Min.setRmssd5MinStart(bedtimeStart);
            //add the activity/minute to the list
            listToSave.add(rmssd5Min);
            //go to the next 5 minutes
            bedtimeStart = bedtimeStart.plusMinutes(5);
        }
        rmssd5MinRepository.saveAll(listToSave);
	}
	
    private int[] jsonArrayToIntArray(JSONArray jsonArray) throws JSONException {
        int[] intArray = new int[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            intArray[i] = jsonArray.getInt(i);
        }
        return intArray;
    }

    private int[] stringSplitIntoIntArray(String stringUnsplitted) {
        int[] stringSplitted = new int[stringUnsplitted.length()];
        for (int i = 0; i < stringUnsplitted.length(); i++) {
            // convert char to int
            stringSplitted[i] = stringUnsplitted.charAt(i) - '0';
        }
        return stringSplitted;
    }
}
