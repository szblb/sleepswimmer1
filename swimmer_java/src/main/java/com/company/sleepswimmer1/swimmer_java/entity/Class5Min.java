package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::class_5min\"")
public class Class5Min {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"class_5min_id\"")
    private String class5MinId;
    
    @Column(name="\"class_5min_value\"")
    private int class5MinValue;
    
    @Column(name="\"class_5min_start\"")
    private ZonedDateTime class5MinStart;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getClass5MinId() {
        return class5MinId;
    }

    public void setClass5MinId(String class5MinId) {
        this.class5MinId = class5MinId;
    }
    
    public int getClass5MinValue() {
        return class5MinValue;
    }

    public void setClass5MinValue(int class5MinValue) {
        this.class5MinValue = class5MinValue;
    }
    
    public ZonedDateTime getClass5MinStart() {
        return class5MinStart;
    }

    public void setClass5MinStart(ZonedDateTime class5MinStart) {
        this.class5MinStart = class5MinStart;
    }
}
