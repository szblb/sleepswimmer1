package com.company.sleepswimmer1.swimmer_java.service;

import com.company.sleepswimmer1.swimmer_java.entity.Activity;
import com.company.sleepswimmer1.swimmer_java.repository.ActivityRepository;
import com.company.sleepswimmer1.swimmer_java.entity.Class5Min;
import com.company.sleepswimmer1.swimmer_java.repository.Class5MinRepository;
import com.company.sleepswimmer1.swimmer_java.entity.Met1Min;
import com.company.sleepswimmer1.swimmer_java.repository.Met1MinRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ActivityService {
    @Autowired
    private ActivityRepository activityRepository;
	
	@Autowired
    private Class5MinRepository class5MinRepository;
    
    @Autowired
    private Met1MinRepository met1MinRepository;
	
	@PersistenceContext
	private EntityManager entityManager;

    /**
     * Saves the activityInfo gathered from the controller.
     * The data in the String is the full data gathered from Oura api - it can mean multiple days
     * So we have to parse the array in the json to insert one by one the lines in the database
     * @param stringActivityInfo
     */
    public void saveMultipleDaysActivityInfo(String stringActivityInfo, String userId) {
        try {
            JSONArray jsonActivityInfo = new JSONObject(stringActivityInfo).getJSONArray("activity");

            final int numberOfItemsInJson = jsonActivityInfo.length();
            for (int i = 0; i < numberOfItemsInJson; i++){
                JSONObject day = jsonActivityInfo.getJSONObject(i);
                this.saveSingleDayActivityFromJson(day, userId);
                System.out.println("Successfully saved a new Activity Info day!");
            }
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }
	
	/**
	 * @param JSONObject the JSONObject containing a day of activity
	 * @param String the String containing the User's dw_id
	 */
    public void saveSingleDayActivityFromJson(JSONObject day, String userId) throws JSONException {
        Activity activity = new Activity();
        
		activity.setUserId(userId);
        activity.setDayEnd(day.getString("day_end"));
        activity.setDayStart(day.getString("day_start"));
        activity.setSummaryDate(day.getString("summary_date"));
        activity.setAverageMet(day.getFloat("average_met"));
        activity.setCalActive(day.getInt("cal_active"));
        activity.setCalTotal(day.getInt("cal_total"));
        activity.setDailyMovement(day.getInt("daily_movement"));
        activity.setInactive(day.getInt("inactive"));
        activity.setInactivityAlerts(day.getInt("inactivity_alerts"));
        activity.setLow(day.getInt("low"));
        activity.setMedium(day.getInt("medium"));
        activity.setHigh(day.getInt("high"));
        activity.setMetMinHigh(day.getInt("met_min_high"));
        activity.setMetMinInactive(day.getInt("met_min_inactive"));
        activity.setMetMinLow(day.getInt("met_min_low"));
        activity.setMetMinMedium(day.getInt("met_min_medium"));
        activity.setMetMinMediumPlus(-1); //TODO: Check api changes
        activity.setNonWear(day.getInt("non_wear"));
        activity.setRest(day.getInt("rest"));
        activity.setScore(day.getInt("score"));
        activity.setScoreMeetDailyTargets(day.getInt("score_meet_daily_targets"));
        activity.setScoreMoveEveryHour(day.getInt("score_move_every_hour"));
        activity.setScoreRecoveryTime(day.getInt("score_recovery_time"));
        activity.setScoreStayActive(day.getInt("score_stay_active"));
        activity.setScoreTrainingFrequency(day.getInt("score_training_frequency"));
        activity.setScoreTrainingVolume(day.getInt("score_training_volume"));
        activity.setSteps(day.getInt("steps"));
        activity.setTimezone(day.getInt("timezone"));
  
        activityRepository.save(activity);
		
		this.saveClass5MinFromJson(day.getString("class_5min"), activity.getClass5MinId(), activity.getDayStart(), userId);
		this.saveMet1MinFromJson(day.getJSONArray("met_1min"), activity.getMet1MinId(), activity.getDayStart(), userId);
    }
	
	/**
	 * @param class5MinString the class5MinString with the format "1231212221110001"
	 */
	@Transactional
	private void saveClass5MinFromJson(String class5MinString, String class5MinId, ZonedDateTime dayStart, String userId) {
		int[] activity5MinArray = this.stringSplitIntoIntArray(class5MinString);
		// list to save it as a bulk
        List<Class5Min> listToSave = new ArrayList<>();
        Class5Min class5Min;
        for (int i = 0; i < activity5MinArray.length; i++) {
            class5Min = new Class5Min();
            class5Min.setUserId(userId);
			class5Min.setClass5MinId(class5MinId);
			class5Min.setClass5MinValue(activity5MinArray[i]);
			class5Min.setClass5MinStart(dayStart);
            //add the activity/minute to the list
            listToSave.add(class5Min);
            //go to the next 5 minutes
            dayStart = dayStart.plusMinutes(5);
        }
        class5MinRepository.saveAll(listToSave);
	}
	@Transactional
	private void saveMet1MinFromJson(JSONArray met1MinJsonArray, String met1MinId, ZonedDateTime dayStart, String userId) {
		double[] activity1MinArray = this.jsonArrayToDoubleArray(met1MinJsonArray);
		// list to save it as a bulk
        List<Met1Min> listToSave = new ArrayList<>();
        Met1Min met1Min;
        for (int i = 0; i < activity1MinArray.length; i++) {
            met1Min = new Met1Min();
            met1Min.setUserId(userId);
			met1Min.setMet1MinId(met1MinId);
			met1Min.setMet1MinValue(activity1MinArray[i]);
			met1Min.setMet1MinStart(dayStart);
            //add the activity/minute to the list
            listToSave.add(met1Min);
            //go to the next 1 minute
            dayStart = dayStart.plusMinutes(1);
        }
        met1MinRepository.saveAll(listToSave);
	}

    private double[] jsonArrayToDoubleArray(JSONArray jsonArray) throws JSONException {
        double[] doubleArray = new double[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
        	doubleArray[i] = jsonArray.getDouble(i);
        }
        return doubleArray;
    }

    private int[] stringSplitIntoIntArray(String stringUnsplitted) {
        int[] stringSplitted = new int[stringUnsplitted.length()];
        for (int i = 0; i < stringUnsplitted.length(); i++) {
            // convert char to int
            stringSplitted[i] = stringUnsplitted.charAt(i) - '0';
        }
        return stringSplitted;
    }
}
