package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.UUID;

/** Activity summary contains daily activity summary values and detailed activity levels.
 * Activity levels are expresses in metabolic-equivalent minutes (MET mins).
 * Oura tracks activity based on the movement.
 * Your Activity Score is an overall measure of how active you've been today, and over the past seven days.
 * Activity contributors are calculated over several days.
 */
@Entity
@Table(name="\"sleepswimmer1.db::activity\"")
public class Activity {
    @Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
	
	@Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"summary_date\"")
    private LocalDate summaryDate; /* Date when the activity period started. Oura activity period is from 4 AM to 3:59 AM user's local time. */

    @Column(name="\"day_start\"")
    private ZonedDateTime dayStart; /* UTC time when the activity day began. Oura activity day is usually from 4AM to 4AM local time. */

    @Column(name="\"day_end\"")
    private ZonedDateTime dayEnd; /* Date time UTC time when the activity day ended. Oura activity day is usually from 4AM to 4AM local time. */

    @Column(name="\"timezone\"")
    private int timezone; /* Timezone offset from UTC as minutes. */

    @Column(name="\"score\"")
    private int score; /* Activity score provides an estimate how well recent physical activity has matched ring user's needs.
                        * It is calculated as a weighted average of activity score contributors that represent one aspect
                        * of suitability of the activity each. The contributor values are also available as separate parameters. */

    @Column(name="\"score_stay_active\"")
    private int scoreStayActive; /* This activity score contributor indicates how well the ring user has managed to
                                  * avoid of inactivity (sitting or standing still) during last 24 hours.
                                  * The more inactivity, the lower contributor value.
                                  * The contributor value is 100 when inactive time during past 24 hours is below 5 hours.
                                  * The contributor value is above 95 when inactive time during past 24 hours is below 7 hours.
                                  * The weight of activity.score_stay_active in activity score calculation is 0.15. */

    @Column(name="\"score_move_every_hour\"")
    private int scoreMoveEveryHour; /* This activity score contributor indicates how well the ring user has managed
                                     * to avoid long periods of inactivity (sitting or standing still) during last 24 hours.
                                     * The contributor includes number of continuous inactive periods of
                                     * 60 minutes or more (excluding sleeping). The more long inactive periods,
                                     * the lower contributor value. The contributor value is 100 when no continuous
                                     * inactive periods of 60 minutes or more have been registered.
                                     * The contributor value is above 95 when at most one continuous inactive period of
                                     * 60 minutes or more has been registered. The weight of activity.score_move_every_hour
                                     * in activity score calculation is 0.10. */

    @Column(name="\"score_meet_daily_targets\"")
    private int scoreMeetDailyTargets; /* This activity score contributor indicates how often the ring user has reached his/her
                                        * daily activity target during seven last days (100 = six or seven times, 95 = five times).
                                        * The weight of activity.score_meet_daily_targets in activity score calculation is 0.25. */

    @Column(name="\"score_training_frequency\"")
    private int scoreTrainingFrequency; /* This activity score contributor indicates how regularly the ring user has
                                         * had physical exercise the ring user has got during last seven days.
                                         * The contributor value is 100 when the user has got more than 100 minutes of
                                         * medium or high intensity activity on at least four days during past seven days.
                                         * The contributor value is 95 when the user has got more than 100 minutes of
                                         * medium or high intensity activity on at least three days during past seven days.
                                         * The weight of activity.score_training_frequency in activity score calculation is 0.10. */

    @Column(name="\"score_training_volume\"")
    private int scoreTrainingVolume; /* This activity score contributor indicates how much physical exercise
                                      * the ring user has got during last seven days.
                                      * The contributor value is 100 when thes sum of weekly MET minutes is over 2000.
                                      * The contributor value is 95 when the sum of weekly MET minutes is over 750.
                                      * There is a weighting function so that the effect of each day gradually disappears.
                                      * The weight of activity.score_training_volume in activity score calculation is 0.15. */

    @Column(name="\"score_recovery_time\"")
    private int scoreRecoveryTime; /* This activity score contributor indicates if the user has got enough recovery time
                                    * during last seven days. The contributor value is 100 when:
                                    * 1. The user has got at least two recovery days during past 7 days.
                                    * 2. No more than two days elapsed after the latest recovery day.
                                    *
                                    * The contributor value is 95 when:
                                    * 1. The user has got at least one recovery day during past 7 days.
                                    * 2. No more than three days elapsed after the latest recovery day.
                                    *
                                    * Here a day is considered as a recovery day when amount of high intensity activity
                                    * did not exceed 100 MET minutes and amount of medium intensity activity
                                    * did not exceed 200 MET minutes. The exact limits will be age and gender dependent.
                                    * The weight of activity.score_recovery_time in activity score calculation is 0.25. */

    @Column(name="\"daily_movement\"")
    private int dailyMovement; /* Daily physical activity as equal meters i.e. amount of walking needed
                                * to get the same amount of activity. */


    @Column(name="\"non_wear\"")
    private int nonWear; /* Number of minutes during the day when the user was not wearing the ring.
                          * Can be used as a proxy for data accuracy, i.e. how well the measured physical activity
                          * represents actual total activity of the ring user. */

    @Column(name="\"rest\"")
    private int rest; /* Number of minutes during the day spent resting i.e. sleeping or lying down
                       * (average MET level of the minute is below 1.05). */

    @Column(name="\"inactive\"")
    private int inactive; /* Number of inactive minutes (sitting or standing still,
                           * average MET level of the minute between 1.05 and 2) during the day. */

    @Column(name="\"inactivity_alerts\"")
    private int inactivityAlerts; /* Number of continuous inactive periods of 60 minutes or more during the day. */

    @Column(name="\"low\"")
    private int low; /* Number of minutes during the day with low intensity activity (e.g. household work,
                      * average MET level of the minute between 2 and age dependent limit). */

    @Column(name="\"medium\"")
    private int medium; /* Number of minutes during the day with medium intensity activity (e.g. walking).
                         * The upper and lower MET level limits for medium intensity activity depend on user's age and gender. */

    @Column(name="\"high\"")
    private int high; /* Number of minutes during the day with high intensity activity (e.g. running).
                       * The lower MET level limit for high intensity activity depends on user's age and gender. */

    @Column(name="\"steps\"")
    private int steps; /* Total number of steps registered during the day. */

    @Column(name="\"cal_total\"")
    private int calTotal; /* Total energy consumption during the day including Basal Metabolic Rate in kilocalories. */

    @Column(name="\"cal_active\"")
    private int calActive; /* Energy consumption caused by the physical activity of the day in kilocalories. */

    @Column(name="\"met_min_inactive\"")
    private int metMinInactive; /* Total MET minutes accumulated during inactive minutes of the day. */

    @Column(name="\"met_min_low\"")
    private int metMinLow; /* Total MET minutes accumulated during low intensity activity minutes of the day. */

    @Column(name="\"met_min_medium_plus\"")
    private int metMinMediumPlus; /* Total MET minutes accumulated during medium and high intensity activity minutes of the day. */

    @Column(name="\"met_min_medium\"")
    private int metMinMedium; /* Total MET minutes accumulated during medium intensity activity minutes of the day. */

    @Column(name="\"met_min_high\"")
    private int metMinHigh; /* Total MET minutes accumulated during high intensity activity minutes of the day. */

    @Column(name="\"average_met\"")
    private float averageMet; /* Average MET level during the whole day. */
	
	@Column(name="\"class_5min_id\"")
    private String class5MinId = UUID.randomUUID().toString();
    
    @Column(name="\"met_1min_id\"")
    private String met1MinId = UUID.randomUUID().toString();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	
	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public LocalDate getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(LocalDate summaryDate) {
        this.summaryDate = summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = LocalDate.parse(summaryDate);
    }

    public ZonedDateTime getDayEnd() {
        return dayEnd;
    }

    public void setDayEnd(ZonedDateTime dayEnd) {
        this.dayEnd = dayEnd;
    }

    public void setDayEnd(String dayEnd) {
        this.dayEnd = ZonedDateTime.parse(dayEnd);
    }

    public ZonedDateTime getDayStart() {
        return dayStart;
    }

    public void setDayStart(ZonedDateTime dayStart) {
        this.dayStart = dayStart;
    }

    public void setDayStart(String dayStart) {
        this.dayStart = ZonedDateTime.parse(dayStart);
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScoreStayActive() {
        return scoreStayActive;
    }

    public void setScoreStayActive(int scoreStayActive) {
        this.scoreStayActive = scoreStayActive;
    }

    public int getScoreMoveEveryHour() {
        return scoreMoveEveryHour;
    }

    public void setScoreMoveEveryHour(int scoreMoveEveryHour) {
        this.scoreMoveEveryHour = scoreMoveEveryHour;
    }

    public int getScoreMeetDailyTargets() {
        return scoreMeetDailyTargets;
    }

    public void setScoreMeetDailyTargets(int scoreMeetDailyTargets) {
        this.scoreMeetDailyTargets = scoreMeetDailyTargets;
    }

    public int getScoreTrainingFrequency() {
        return scoreTrainingFrequency;
    }

    public void setScoreTrainingFrequency(int scoreTrainingFrequency) {
        this.scoreTrainingFrequency = scoreTrainingFrequency;
    }

    public int getScoreRecoveryTime() {
        return scoreRecoveryTime;
    }

    public void setScoreRecoveryTime(int scoreRecoveryTime) {
        this.scoreRecoveryTime = scoreRecoveryTime;
    }

    public int getScoreTrainingVolume() {
        return scoreTrainingVolume;
    }

    public void setScoreTrainingVolume(int scoreTrainingVolume) {
        this.scoreTrainingVolume = scoreTrainingVolume;
    }

    public int getDailyMovement() {
        return dailyMovement;
    }

    public void setDailyMovement(int dailyMovement) {
        this.dailyMovement = dailyMovement;
    }

    public int getNonWear() {
        return nonWear;
    }

    public void setNonWear(int nonWear) {
        this.nonWear = nonWear;
    }

    public int getRest() {
        return rest;
    }

    public void setRest(int rest) {
        this.rest = rest;
    }

    public int getInactive() {
        return inactive;
    }

    public void setInactive(int inactive) {
        this.inactive = inactive;
    }

    public int getLow() {
        return low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getInactivityAlerts() {
        return inactivityAlerts;
    }

    public void setInactivityAlerts(int inactivityAlerts) {
        this.inactivityAlerts = inactivityAlerts;
    }

    public int getMedium() {
        return medium;
    }

    public void setMedium(int medium) {
        this.medium = medium;
    }

    public int getHigh() {
        return high;
    }
	
	public void setHigh(int high) {
        this.high = high;
    }
    
    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public int getCalTotal() {
        return calTotal;
    }

    public void setCalTotal(int calTotal) {
        this.calTotal = calTotal;
    }

    public int getCalActive() {
        return calActive;
    }

    public void setCalActive(int calActive) {
        this.calActive = calActive;
    }

    public int getMetMinInactive() {
        return metMinInactive;
    }

    public void setMetMinInactive(int metMinInactive) {
        this.metMinInactive = metMinInactive;
    }

    public int getMetMinLow() {
        return metMinLow;
    }

    public void setMetMinLow(int metMinLow) {
        this.metMinLow = metMinLow;
    }

    public int getMetMinMediumPlus() {
        return metMinMediumPlus;
    }

    public void setMetMinMediumPlus(int metMinMediumPlus) {
        this.metMinMediumPlus = metMinMediumPlus;
    }

    public int getMetMinMedium() {
        return metMinMedium;
    }

    public void setMetMinMedium(int metMinMedium) {
        this.metMinMedium = metMinMedium;
    }

    public int getMetMinHigh() {
        return metMinHigh;
    }

    public void setMetMinHigh(int metMinHigh) {
        this.metMinHigh = metMinHigh;
    }

    public float getAverageMet() {
        return averageMet;
    }

    public void setAverageMet(float averageMet) {
        this.averageMet = averageMet;
    }
	
	public String getClass5MinId() {
        return class5MinId;
    }

    public void setClass5MinId(String class5MinId) {
        this.class5MinId = class5MinId;
    }
    
    public String getMet1MinId() {
        return met1MinId;
    }

    public void setMet1MinId(String met1MinId) {
        this.met1MinId = met1MinId;
    }
}
