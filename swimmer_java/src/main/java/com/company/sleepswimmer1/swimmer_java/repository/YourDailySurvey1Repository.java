package com.company.sleepswimmer1.swimmer_java.repository;

import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey1;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;

@Repository
public interface YourDailySurvey1Repository  extends CrudRepository<YourDailySurvey1, String> {
	YourDailySurvey1 findByUserIdAndSurveyDay(String userId, LocalDate surveyDay);
}
