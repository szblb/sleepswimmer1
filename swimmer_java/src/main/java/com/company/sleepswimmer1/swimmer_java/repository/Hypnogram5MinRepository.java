package com.company.sleepswimmer1.swimmer_java.repository;

import com.company.sleepswimmer1.swimmer_java.entity.Hypnogram5Min;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Hypnogram5MinRepository extends CrudRepository<Hypnogram5Min, String> {
}
