package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::sleep\"")
public class Sleep {
    @Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
	
	@Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"summary_date\"")
    private LocalDate summaryDate;

    @Column(name="\"period_id\"")
    private int periodId;

    @Column(name="\"is_longest\"")
    private int isLongest;

    @Column(name="\"timezone\"")
    private int timezone; /* Timezone offset from UTC as minutes. */
	
	@Column(name="\"bedtime_start\"")
    private ZonedDateTime bedtimeStart;

    @Column(name="\"bedtime_end\"")
    private ZonedDateTime bedtimeEnd;

    @Column(name="\"score\"")
    private int score;
    
    @Column(name="\"score_total\"")
    private int scoreTotal;
    
    @Column(name="\"score_disturbances\"")
    private int scoreDisturbances;

	@Column(name="\"score_efficiency\"")
    private int scoreEfficiency;
    
    @Column(name="\"score_latency\"")
    private int scoreLatency;
    
    @Column(name="\"score_rem\"")
    private int scoreRem;
    
    @Column(name="\"score_deep\"")
    private int scoreDeep;
    
    @Column(name="\"score_alignment\"")
    private int scoreAlignment;
    
    @Column(name="\"total\"")
    private int total;
    
    @Column(name="\"duration\"")
    private int duration;
    
    @Column(name="\"awake\"")
    private int awake;
    
    @Column(name="\"light\"")
    private int light;
    
    @Column(name="\"rem\"")
    private int rem;
    
    @Column(name="\"deep\"")
    private int deep;
    
    @Column(name="\"onset_latency\"")
    private int onsetLatency;
    
    @Column(name="\"restless\"")
    private int restless;
    
    @Column(name="\"efficiency\"")
    private int efficiency;
    
    @Column(name="\"midpoint_time\"")
    private int midpointTime;
    
    @Column(name="\"hr_lowest\"")
    private int hrLowest;
    
    @Column(name="\"hr_average\"")
    private float hrAverage;
    
    @Column(name="\"rmssd\"")
    private int rmssd;
    
    @Column(name="\"breath_average\"")
    private float breathAverage;
    
    @Column(name="\"temperature_delta\"")
    private double temperatureDelta;
    
    @Column(name="\"hypnogram_5min_id\"")
    private String hypnogram5MinId;
    
    @Column(name="\"hr_5min_id\"")
    private String hr5MinId;
    
    @Column(name="\"rmssd_5min_id\"")
    private String rmssd5MinId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
	
	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public LocalDate getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(LocalDate summaryDate) {
        this.summaryDate = summaryDate;
    }

    public void setSummaryDate(String summaryDate) {
        this.summaryDate = LocalDate.parse(summaryDate);
    }

    public int getPeriodId() {
        return periodId;
    }

    public void setPeriodId(int periodId) {
        this.periodId = periodId;
    }

    public void setIsLongest(int isLongest) {
        this.isLongest = isLongest;
    }

    public int getIsLongest() {
        return isLongest;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

	public ZonedDateTime getBedtimeStart() {
		return bedtimeStart;
	}
	
	public void setBedtimeStart(ZonedDateTime bedtimeStart) {
		this.bedtimeStart = bedtimeStart;
	}
	
	public ZonedDateTime getBedtimeEnd() {
		return bedtimeEnd;
	}
	
	public void setBedtimeEnd(ZonedDateTime bedtimeEnd) {
		this.bedtimeEnd = bedtimeEnd;
	}
	
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    public int getScoreTotal() {
        return scoreTotal;
    }

    public void setScoreTotal(int scoreTotal) {
        this.scoreTotal = scoreTotal;
    }
    
    public int getScoreDisturbances() {
        return scoreDisturbances;
    }

    public void setScoreDisturbances(int scoreDisturbances) {
        this.scoreDisturbances = scoreDisturbances;
    }

	public int getScoreEfficiency() {
        return scoreEfficiency;
    }

    public void setScoreEfficiency(int scoreEfficiency) {
        this.scoreEfficiency = scoreEfficiency;
    }
	
	public int getScoreLatency() {
        return scoreLatency;
    }

    public void setScoreLatency(int scoreLatency) {
        this.scoreLatency = scoreLatency;
    }

	public int getScoreRem() {
        return scoreRem;
    }

    public void setScoreRem(int scoreRem) {
        this.scoreRem = scoreRem;
    }

	public int getScoreDeep() {
        return scoreDeep;
    }

    public void setScoreDeep(int scoreDeep) {
        this.scoreDeep = scoreDeep;
    }
    
    public int getScoreAlignment() {
        return scoreAlignment;
    }

    public void setScoreAlignment(int scoreAlignment) {
        this.scoreAlignment = scoreAlignment;
    }
    
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
	
	public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

	public int getAwake() {
        return awake;
    }

    public void setAwake(int awake) {
        this.awake = awake;
    }
    
    public int getLight() {
        return light;
    }

    public void setLight(int light) {
        this.light = light;
    }
	
	public int getRem() {
        return rem;
    }

    public void setRem(int rem) {
        this.rem = rem;
    }
    
    public int getDeep() {
        return deep;
    }

    public void setDeep(int deep) {
        this.deep = deep;
    }
    
    public int getOnsetLatency() {
        return onsetLatency;
    }

    public void setOnsetLatency(int onsetLatency) {
        this.onsetLatency = onsetLatency;
    }
    
    public int getRestless() {
        return restless;
    }

    public void setRestless(int restless) {
        this.restless = restless;
    }
    
    public int getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(int efficiency) {
        this.efficiency = efficiency;
    }
    
    public int getMidpointTime() {
        return midpointTime;
    }

    public void setMidpointTime(int midpointTime) {
        this.midpointTime = midpointTime;
    }
    
    public int getHrLowest() {
        return hrLowest;
    }

    public void setHrLowest(int hrLowest) {
        this.hrLowest = hrLowest;
    }
    
    public float getHrAverage() {
        return hrLowest;
    }

    public void setHrAverage(float hrAverage) {
        this.hrAverage = hrAverage;
    }
    
    public int getRmssd() {
        return rmssd;
    }

    public void setRmssd(int rmssd) {
        this.rmssd = rmssd;
    }
	
	public float getBreathAverage() {
        return breathAverage;
    }

    public void setBreathAverage(float breathAverage) {
        this.breathAverage = breathAverage;
    }
    
    public double getTemperatureDelta() {
        return temperatureDelta;
    }

    public void setTemperatureDelta(double temperatureDelta) {
        this.temperatureDelta = temperatureDelta;
    }
    
    public String getHypnogram5MinId() {
        return hypnogram5MinId;
    }

    public void setHypnogram5MinId(String hypnogram5MinId) {
        this.hypnogram5MinId = hypnogram5MinId;
    }
    
    public String getHr5MinId() {
        return hr5MinId;
    }

    public void setHr5MinId(String hr5MinId) {
        this.hr5MinId = hr5MinId;
    }
    
    public String getRmssd5MinId() {
        return rmssd5MinId;
    }

    public void setRmssd5MinId(String rmssd5MinId) {
        this.rmssd5MinId = rmssd5MinId;
    }
}
