package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::yourdailysurvey2\"")
public class YourDailySurvey2 {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"survey_day\"")
    private LocalDate surveyDay;
    
    @Column(name="\"sleep_well_score\"")
    private int sleepWellScore;
	
	@Column(name="\"physical_well_score\"")
    private int physicalWellScore;
    
    @Column(name="\"feel_motivated_score\"")
    private int feelMotivatedScore;
    
    public YourDailySurvey2() {
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
    public String getId() {
    	return id;
    }
    
    public void setUserId(String userId) {
    	this.userId = userId;
    }
    
    public String getUserId() {
    	return userId;
    }
    
    public void setSurveyDay(LocalDate surveyDay) {
    	this.surveyDay = surveyDay;
    }
    
    public LocalDate getSurveyDay() {
    	return surveyDay;
    }
    
    public void setSleepWellScore(int sleepWellScore) {
    	this.sleepWellScore = sleepWellScore;
    }
    
    public int getSleepWellScore() {
    	return sleepWellScore;
    }
    
    public void setPhysicalWellScore(int physicalWellScore) {
    	this.physicalWellScore = physicalWellScore;
    }
    
    public int getPhysicalWellScore() {
    	return physicalWellScore;
    }
    
    public void setFeelMotivatedScore(int feelMotivatedScore) {
    	this.feelMotivatedScore = feelMotivatedScore;
    }
    
    public int getFeelMotivatedScore() {
    	return feelMotivatedScore;
    }
}
