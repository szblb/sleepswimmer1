package com.company.sleepswimmer1.swimmer_java.repository;

import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey3;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;

@Repository
public interface YourDailySurvey3Repository  extends CrudRepository<YourDailySurvey3, String> {
	YourDailySurvey3 findByUserIdAndSurveyDay(String userId, LocalDate surveyDay);
}
