package com.company.sleepswimmer1.swimmer_java.service;

import com.company.sleepswimmer1.swimmer_java.entity.Readiness;
import com.company.sleepswimmer1.swimmer_java.repository.ReadinessRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ReadinessService {
    @Autowired
    private ReadinessRepository readinessRepository;

    public void saveMultipleDaysReadinessInfo(String stringReadinessInfo, String userId) {
        try {
            JSONArray jsonReadinessInfo = new JSONObject(stringReadinessInfo).getJSONArray("readiness");

            final int numberOfItemsInJson = jsonReadinessInfo.length();
            for (int i = 0; i < numberOfItemsInJson; i++){
                JSONObject day = jsonReadinessInfo.getJSONObject(i);
                this.saveSingleDayReadinessFromJson(day, userId);
                System.out.println("Successfully saved a new Readiness Info day!");
            }
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    public void saveSingleDayReadinessFromJson(JSONObject day, String userId) throws JSONException {
        Readiness readiness = new Readiness();
        
		readiness.setUserId(userId);
		readiness.setSummaryDate(day.getString("summary_date"));
		readiness.setPeriodId(day.getInt("period_id"));
		readiness.setScore(day.getInt("score"));
		readiness.setScorePreviousNight(day.getInt("score_previous_night"));
		readiness.setScoreSleepBalance(day.getInt("score_sleep_balance"));
		readiness.setScorePreviousDay(day.getInt("score_previous_day"));
		readiness.setScoreActivityBalance(day.getInt("score_activity_balance"));
		readiness.setScoreRestingHr(day.getInt("score_resting_hr"));
		readiness.setScoreRecoveryIndex(day.getInt("score_recovery_index"));
		readiness.setScoreTemperature(day.getInt("score_temperature"));
		
        readinessRepository.save(readiness);
    }
}
