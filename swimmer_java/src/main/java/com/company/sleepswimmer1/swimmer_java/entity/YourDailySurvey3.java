package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::yourdailysurvey3\"")
public class YourDailySurvey3 {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"survey_day\"")
    private LocalDate surveyDay;
    
    @Column(name="\"liters_of_water\"")
    private int litersOfWater;
	
	@Column(name="\"fruits_and_vegetables\"")
    private int fruitsAndVegetables;
    
    @Column(name="\"proteins\"")
    private int proteins;
    
    @Column(name="\"carbs\"")
    private int carbs;
    
    @Column(name="\"snacks\"")
    private int snacks;
    
    public YourDailySurvey3() {
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
    public String getId() {
    	return id;
    }
    
    public void setUserId(String userId) {
    	this.userId = userId;
    }
    
    public String getUserId() {
    	return userId;
    }
    
    public void setSurveyDay(LocalDate surveyDay) {
    	this.surveyDay = surveyDay;
    }
    
    public LocalDate getSurveyDay() {
    	return surveyDay;
    }
    
    public void setLitersOfWater(int litersOfWater) {
    	this.litersOfWater = litersOfWater;
    }
    
    public int getLitersOfWater() {
    	return litersOfWater;
    }
    
    public void setFruitsAndVegetables(int fruitsAndVegetables) {
    	this.fruitsAndVegetables = fruitsAndVegetables;
    }
    
    public int getFruitsAndVegetables() {
    	return fruitsAndVegetables;
    }
    
    public void setProteins(int proteins) {
    	this.proteins = proteins;
    }
    
    public int getProteins() {
    	return proteins;
    }
    
    public void setCarbs(int carbs) {
    	this.carbs = carbs;
    }
    
    public int getCarbs() {
    	return carbs;
    }
    
    public void setSnacks(int snacks) {
    	this.snacks = snacks;
    }
    
    public int getSnacks() {
    	return snacks;
    }
}
