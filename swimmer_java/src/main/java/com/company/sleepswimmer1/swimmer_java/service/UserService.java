package com.company.sleepswimmer1.swimmer_java.service;

import com.company.sleepswimmer1.swimmer_java.entity.User;
import com.company.sleepswimmer1.swimmer_java.repository.UserRepository;
import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey1;
import com.company.sleepswimmer1.swimmer_java.repository.YourDailySurvey1Repository;
import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey2;
import com.company.sleepswimmer1.swimmer_java.repository.YourDailySurvey2Repository;
import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey3;
import com.company.sleepswimmer1.swimmer_java.repository.YourDailySurvey3Repository;
import com.company.sleepswimmer1.swimmer_java.entity.CoachDailySurvey;
import com.company.sleepswimmer1.swimmer_java.repository.CoachDailySurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private YourDailySurvey1Repository yourDailySurvey1Repository;
    
    @Autowired
    private YourDailySurvey2Repository yourDailySurvey2Repository;
    
    @Autowired
    private YourDailySurvey3Repository yourDailySurvey3Repository;
    
    @Autowired
    private CoachDailySurveyRepository coachDailySurveyRepository;

    public String getFirstNameById(String id) {
        User user = userRepository.findById(id).orElse(new User());
        return user.getFirstName();
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    
    public void save(User user) {
        userRepository.save(user);
    }
    
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }
    
    public YourDailySurvey1 getYourDailySurvey1ByUserIdAndSurveyDay(String userId, LocalDate surveyDay) {
        return yourDailySurvey1Repository.findByUserIdAndSurveyDay(userId, surveyDay);
    }
    
    public void save(YourDailySurvey1 yourDailySurvey1) {
        yourDailySurvey1Repository.save(yourDailySurvey1);
    }
    
    public YourDailySurvey2 getYourDailySurvey2ByUserIdAndSurveyDay(String userId, LocalDate surveyDay) {
        return yourDailySurvey2Repository.findByUserIdAndSurveyDay(userId, surveyDay);
    }
    
    public void save(YourDailySurvey2 yourDailySurvey2) {
        yourDailySurvey2Repository.save(yourDailySurvey2);
    }
    
    public YourDailySurvey3 getYourDailySurvey3ByUserIdAndSurveyDay(String userId, LocalDate surveyDay) {
        return yourDailySurvey3Repository.findByUserIdAndSurveyDay(userId, surveyDay);
    }
    
    public void save(YourDailySurvey3 yourDailySurvey3) {
        yourDailySurvey3Repository.save(yourDailySurvey3);
    }
    
    public CoachDailySurvey getCoachDailySurveyByUserIdAndSurveyDay(String userId, LocalDate surveyDay) {
        return coachDailySurveyRepository.findByUserIdAndSurveyDay(userId, surveyDay);
    }
    
    public void save(CoachDailySurvey coachDailySurvey) {
        coachDailySurveyRepository.save(coachDailySurvey);
    }
}
