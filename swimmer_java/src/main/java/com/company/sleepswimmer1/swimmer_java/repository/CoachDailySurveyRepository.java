package com.company.sleepswimmer1.swimmer_java.repository;

import com.company.sleepswimmer1.swimmer_java.entity.CoachDailySurvey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;

@Repository
public interface CoachDailySurveyRepository  extends CrudRepository<CoachDailySurvey, String> {
	CoachDailySurvey findByUserIdAndSurveyDay(String userId, LocalDate surveyDay);
}
