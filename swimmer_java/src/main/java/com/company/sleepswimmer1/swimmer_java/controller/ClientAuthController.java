package com.company.sleepswimmer1.swimmer_java.controller;

import com.company.sleepswimmer1.swimmer_java.entity.User;
import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey1;
import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey2;
import com.company.sleepswimmer1.swimmer_java.entity.YourDailySurvey3;
import com.company.sleepswimmer1.swimmer_java.entity.CoachDailySurvey;
import com.company.sleepswimmer1.swimmer_java.service.ActivityService;
import com.company.sleepswimmer1.swimmer_java.service.ReadinessService;
import com.company.sleepswimmer1.swimmer_java.service.SleepService;
import com.company.sleepswimmer1.swimmer_java.service.UserService;
import com.sap.cloud.security.xsuaa.token.Token;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
@EnableOAuth2Client
public class ClientAuthController {

    @Autowired
    private UserService userService;

    @Autowired
    ActivityService activityService;

    @Autowired
    ReadinessService readinessService;

    @Autowired
    SleepService sleepService;

    private static final String CLIENT_ID = "sap.oauth2.client.clientId";
    private static final String CLIENT_SECRET = "sap.oauth2.client.clientSecret";
    private static final String GRANT_TYPE = "sap.oauth2.client.grantType";
    private static final String AUTHORIZATION_URI = "sap.oauth2.client.userAuthorizationUri";
    private static final String ACCESS_TOKEN_URI = "sap.oauth2.client.accessTokenUri";
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String UPDATE_TOKEN_SCHEDULER = "0 0 0/11 1/1 * ?";
    private static final String UPDATE_OURA_DATA_SCHEDULER = "0 30 0/11 1/1 * ?";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String BEGINNING = "2019-01-01";



    @Autowired
    private Environment env;

    /**
     * If user allowed the application to acces Oura Api, then we return "OK", else we return "NOK".
     * The response should be handled by the client.
     */
    @GetMapping(value = "/checkIfUserAllowedOura")
    public ResponseEntity<String> checkIfUserAllowedOura(@AuthenticationPrincipal Token token) {
        String response = "NOK";
        User user = userService.getUserByEmail(token.getEmail());
        if (user != null && user.getOuraAllowed()) {
            response = "OK";
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    
    @GetMapping(value = "/checkIfUserSwProfileCompleted")
    public ResponseEntity<String> checkIfUserSwProfileCompleted(@AuthenticationPrincipal Token token) {
    	String response = "NOK";
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user != null && user.getIsSwProfileCompleted()) {
    		response = "OK";
    	}
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    

    @GetMapping(value = "/getUserSwimmerOrCoach")
    public ResponseEntity<String> getUserSwimmerOrCoach(@AuthenticationPrincipal Token token) {
        String response = "Nothing";
        User user = userService.getUserByEmail(token.getEmail());
        if (user != null) {
            response = this.getUserTypeAsString(user);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/setUserSwimmer")
    public ResponseEntity<String> setUserSwimmer(@AuthenticationPrincipal Token token) {
        User user = userService.getUserByEmail(token.getEmail());
        if (user != null) {
            user.setIsSwimmer(true);
            userService.save(user);
        } else {
            user = new User();
            user.setIsSwimmer(true);
            user.setEmail(token.getEmail());
            user.setFirstName(token.getFamilyName());
            user.setLastName(token.getGivenName());
            userService.save(user);
        }
        String response = this.getUserTypeAsString(user);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/setUserCoach")
    public ResponseEntity<String> setUserCoach(@AuthenticationPrincipal Token token) {
        User user = userService.getUserByEmail(token.getEmail());
        if (user != null) {
            user.setIsCoach(true);
            userService.save(user);
        } else {
            user = new User();
            user.setIsCoach(true);
            user.setEmail(token.getEmail());
            user.setFirstName(token.getFamilyName());
            user.setLastName(token.getGivenName());
            userService.save(user);
        }
        String response = this.getUserTypeAsString(user);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    
    @GetMapping(value = "/setUserSwimmerAndCoach")
    public ResponseEntity<String> setUserSwimmerAndCoach(@AuthenticationPrincipal Token token) {
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user != null) {
    		user.setIsSwimmer(true);
    		user.setIsCoach(true);
    		userService.save(user);
    	} else {
    		user = new User();
    		user.setIsSwimmer(true);
    		user.setIsCoach(true);
    		user.setEmail(token.getEmail());
        	user.setFirstName(token.getFamilyName());
        	user.setLastName(token.getGivenName());
        	userService.save(user);
    	}
    	String response = this.getUserTypeAsString(user);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @GetMapping(value = "/setUserInfo")
    public ResponseEntity<String> setUserInfo(@AuthenticationPrincipal Token token, @RequestParam String name,
    	@RequestParam String lastName, @RequestParam String birthday, @RequestParam String gender, @RequestParam int height,
    	@RequestParam int weight, @RequestParam String specialityStroke, @RequestParam int specialityDistance, 
    	@RequestParam int specialityReferenceStrokes) {
    		
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user == null) {
    		user = new User();
    		user.setIsSwimmer(true);
    		user.setEmail(token.getEmail());
        	user.setFirstName(token.getFamilyName());
        	user.setLastName(token.getGivenName());
    	}
    	user.setIsSwProfileCompleted(true);
    	user.setSwProfileName(name);
    	user.setSwProfileLastName(lastName);
    	user.setSwProfileBirthday(LocalDate.parse(birthday));
    	user.setSwProfileGender(gender);
    	user.setSwProfileHeight(height);
    	user.setSwProfileWeight(weight);
    	user.setSwProfileSpecialityStroke(specialityStroke);
    	user.setSwProfileSpecialityDistance(specialityDistance);
    	user.setSwProfileSpecialityReferenceStrokes(specialityReferenceStrokes);
    	userService.save(user);
    	return new ResponseEntity<>("OK", HttpStatus.OK);
    }
    
    @GetMapping(value = "/getUserInfo")
    public ResponseEntity<String> getUserInfo(@AuthenticationPrincipal Token token) {
    		
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user == null || !user.getIsSwProfileCompleted()) {
    		return new ResponseEntity<>("NOK", HttpStatus.OK); 
    	}
    	Map<String,String> info = new HashMap<>();
    	info.put("name", user.getSwProfileName());
    	info.put("lastName", user.getSwProfileLastName());
    	info.put("birthday", user.getSwProfileBirthday().toString());
    	info.put("gender", user.getSwProfileGender());
    	info.put("height", Integer.toString(user.getSwProfileHeight()));
    	info.put("weight", Integer.toString(user.getSwProfileWeight()));
    	info.put("specialityStroke", user.getSwProfileSpecialityStroke());
    	info.put("specialityDistance", Integer.toString(user.getSwProfileSpecialityDistance()));
    	info.put("specialityReferenceStrokes", Integer.toString(user.getSwProfileSpecialityReferenceStrokes()));
    	return new ResponseEntity<>(new JSONObject(info).toString(), HttpStatus.OK);
    }
    
    @GetMapping(value = "/setUserDailySurvey1")
    public ResponseEntity<String> setUserDailySurvey1(@AuthenticationPrincipal Token token, @RequestParam String surveyDay, 
    	@RequestParam String specialityBestTime, @RequestParam String bestDistancePerObjective, 
    	@RequestParam String specialityNumberOfStrokes, @RequestParam String personalSwimmingDistance) {
    		
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user == null) {
    		return new ResponseEntity<>("NOK", HttpStatus.OK); 
    	}
    	YourDailySurvey1 yourDailySurvey1 = userService.getYourDailySurvey1ByUserIdAndSurveyDay(user.getId(), LocalDate.parse(surveyDay));
    	if(yourDailySurvey1 == null) {
    		// no survey was found for given day for that user
    		yourDailySurvey1 = new YourDailySurvey1();
    		yourDailySurvey1.setUserId(user.getId());
    		yourDailySurvey1.setSurveyDay(LocalDate.parse(surveyDay));
    		yourDailySurvey1.setSpecialityBestTime(Double.parseDouble(specialityBestTime));
    		yourDailySurvey1.setBestDistancePerObjective(Double.parseDouble(bestDistancePerObjective));
    		yourDailySurvey1.setSpecialityNumberOfStrokes(Double.parseDouble(specialityNumberOfStrokes));
    		yourDailySurvey1.setPersonalSwimmingDistance(Double.parseDouble(personalSwimmingDistance));
    		userService.save(yourDailySurvey1);
    		return new ResponseEntity<>("OK", HttpStatus.OK);
    	} else {
    		// survey was found for given day for that user, so we must reject the new one as only one can be completed per day
    		return new ResponseEntity<>("NOK", HttpStatus.OK);
    	}
    }
    
    @GetMapping(value = "/setUserDailySurvey2")
    public ResponseEntity<String> setUserDailySurvey2(@AuthenticationPrincipal Token token, @RequestParam String surveyDay, 
    	@RequestParam String sleepWellScore, @RequestParam String physicalWellScore, 
    	@RequestParam String feelMotivatedScore) {
    		
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user == null) {
    		return new ResponseEntity<>("NOK", HttpStatus.OK); 
    	}
    	YourDailySurvey2 yourDailySurvey2 = userService.getYourDailySurvey2ByUserIdAndSurveyDay(user.getId(), LocalDate.parse(surveyDay));
    	if(yourDailySurvey2 == null) {
    		// no survey was found for given day for that user
    		yourDailySurvey2 = new YourDailySurvey2();
    		yourDailySurvey2.setUserId(user.getId());
    		yourDailySurvey2.setSurveyDay(LocalDate.parse(surveyDay));
    		yourDailySurvey2.setSleepWellScore(Integer.parseInt(sleepWellScore));
    		yourDailySurvey2.setPhysicalWellScore(Integer.parseInt(physicalWellScore));
    		yourDailySurvey2.setFeelMotivatedScore(Integer.parseInt(feelMotivatedScore));
    		userService.save(yourDailySurvey2);
    		return new ResponseEntity<>("OK", HttpStatus.OK);
    	} else {
    		// survey was found for given day for that user, so we must reject the new one as only one can be completed per day
    		return new ResponseEntity<>("NOK", HttpStatus.OK);
    	}
    }
    
    @GetMapping(value = "/setUserDailySurvey3")
    public ResponseEntity<String> setUserDailySurvey3(@AuthenticationPrincipal Token token, @RequestParam String surveyDay, 
    	@RequestParam String litersOfWater, @RequestParam String fruitsAndVegetables, 
    	@RequestParam String proteins, @RequestParam String carbs, @RequestParam String snacks) {
    		
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user == null) {
    		return new ResponseEntity<>("NOK", HttpStatus.OK); 
    	}
    	YourDailySurvey3 yourDailySurvey3 = userService.getYourDailySurvey3ByUserIdAndSurveyDay(user.getId(), LocalDate.parse(surveyDay));
    	if(yourDailySurvey3 == null) {
    		// no survey was found for given day for that user
    		yourDailySurvey3 = new YourDailySurvey3();
    		yourDailySurvey3.setUserId(user.getId());
    		yourDailySurvey3.setSurveyDay(LocalDate.parse(surveyDay));
    		yourDailySurvey3.setLitersOfWater(Integer.parseInt(litersOfWater));
    		yourDailySurvey3.setFruitsAndVegetables(Integer.parseInt(fruitsAndVegetables));
    		yourDailySurvey3.setProteins(Integer.parseInt(proteins));
    		yourDailySurvey3.setCarbs(Integer.parseInt(carbs));
    		yourDailySurvey3.setSnacks(Integer.parseInt(snacks));
    		userService.save(yourDailySurvey3);
    		return new ResponseEntity<>("OK", HttpStatus.OK);
    	} else {
    		// survey was found for given day for that user, so we must reject the new one as only one can be completed per day
    		return new ResponseEntity<>("NOK", HttpStatus.OK);
    	}
    }
    
    @GetMapping(value = "/setCoachDailySurvey")
    public ResponseEntity<String> setCoachDailySurvey(@AuthenticationPrincipal Token token, @RequestParam String surveyDay, 
    	@RequestParam String scheduleName, @RequestParam String swimmingDistance, 
    	@RequestParam String trainingIntensity, @RequestParam String duration, @RequestParam String comments) {
    		
    	User user = userService.getUserByEmail(token.getEmail());
    	if(user == null) {
    		return new ResponseEntity<>("NOK", HttpStatus.OK); 
    	}
    	CoachDailySurvey coachDailySurvey = userService.getCoachDailySurveyByUserIdAndSurveyDay(user.getId(), LocalDate.parse(surveyDay));
    	if(coachDailySurvey == null) {
    		// no survey was found for given day for that user
    		coachDailySurvey = new CoachDailySurvey();
    		coachDailySurvey.setUserId(user.getId());
    		coachDailySurvey.setSurveyDay(LocalDate.parse(surveyDay));
    		coachDailySurvey.setScheduleName(scheduleName);
    		coachDailySurvey.setSwimmingDistance(Double.parseDouble(swimmingDistance));
    		coachDailySurvey.setTrainingIntensity(trainingIntensity);
    		coachDailySurvey.setDuration(Double.parseDouble(duration));
    		coachDailySurvey.setComments(comments);
    		userService.save(coachDailySurvey);
    		return new ResponseEntity<>("OK", HttpStatus.OK);
    	} else {
    		// survey was found for given day for that user, so we must reject the new one as only one can be completed per day
    		return new ResponseEntity<>("NOK", HttpStatus.OK);
    	}
    }
    
    @GetMapping(value = "/ouraLogin")
    ResponseEntity<String> ouraLogin(@AuthenticationPrincipal Token token) {
    	//TODO: save "state" to user db in order to check the match when the user will request "/callback"
    	//for security
		AuthorizationCodeResourceDetails authorizationCodeResourceDetails = oAuthDetails();
		OAuth2RestTemplate request = new OAuth2RestTemplate(authorizationCodeResourceDetails);
        return request.exchange(env.getProperty(AUTHORIZATION_URI), HttpMethod.GET, null, String.class);
    }

    @GetMapping(value = "/callback")
    public ResponseEntity<String> getDashboard(@AuthenticationPrincipal Token token, @RequestParam String code, @RequestParam String state) {
        //TODO: refactor this, move code to service...

        User user = getUser(token, getTokenInfo(code, state));

        // This shouldn't be here, it's for TESTING purposes,
        //basically this grabs all the data from the activity+readiness info and saves it in the database,
        //frontend will be busy waiting for this data.
        //TODO: Think of an way to get this data at intervals, maybe cron-job, maybe user requests,
        //maybe do some multi-threading or atleast on different thread.

        grabLastOuraData(user);


        return new ResponseEntity<>("You allowed Sleepswimmer to access your Oura Ring API data. Thanks!", HttpStatus.OK);
    }

    private void grabLastOuraData(User user) {
        String startDate=user.getLastUpdated()!=null?user.getLastUpdated(): BEGINNING;
        String endDate= new SimpleDateFormat(DATE_PATTERN).format(new Date());

        saveActivityInfo(user,startDate,endDate);
        saveReadinessInfo(user,startDate,endDate);
        saveSleepInfo(user,startDate,endDate);

        user.setLastUpdated(endDate);
        userService.save(user);
    }

    private JSONObject getTokenInfo(@RequestParam String code, @RequestParam String state) {
        MultiValueMap<String, String> bodyParamMap = new LinkedMultiValueMap<>();
        bodyParamMap.add("code", code);
        bodyParamMap.add("state", state);
        bodyParamMap.add("grant_type", env.getProperty(GRANT_TYPE));
        bodyParamMap.add("client_id", env.getProperty(CLIENT_ID));
        bodyParamMap.add("client_secret", env.getProperty(CLIENT_SECRET));
        return new JSONObject(new RestTemplate().postForEntity(env.getProperty(ACCESS_TOKEN_URI), bodyParamMap, String.class).getBody());
    }

    private void renewAccessToken(User user) {
        //TODO have to check the renewal flow
        MultiValueMap<String, String> bodyParamMap = new LinkedMultiValueMap<>();
        bodyParamMap.add("grant_type", env.getProperty(REFRESH_TOKEN));
        bodyParamMap.add("client_id", env.getProperty(CLIENT_ID));
        bodyParamMap.add("client_secret", env.getProperty(CLIENT_SECRET));
        bodyParamMap.add(REFRESH_TOKEN, user.getRefreshToken());

        JSONObject tokenInfoResponse = new JSONObject(new RestTemplate().postForEntity(env.getProperty(ACCESS_TOKEN_URI), bodyParamMap, String.class).getBody());

        user.setAccessToken(tokenInfoResponse.getString(ACCESS_TOKEN));
        user.setRefreshToken(tokenInfoResponse.getString(REFRESH_TOKEN));
        userService.save(user);
    }

    private User getUser(@AuthenticationPrincipal Token token, JSONObject tokenInfoJson) {
        User user = userService.getUserByEmail(token.getEmail());
        if (user == null) {
            user = new User();
        }
        user.setEmail(token.getEmail());
        user.setFirstName(token.getFamilyName());
        user.setLastName(token.getGivenName());
        user.setOuraAllowed(true);
        user.setAccessToken(tokenInfoJson.getString(ACCESS_TOKEN));
        user.setRefreshToken(tokenInfoJson.getString(REFRESH_TOKEN));
        userService.save(user);
        return user;
    }

    private void saveSleepInfo(User user, String startDate, String endDate) {
        String result = getResult(user, startDate, endDate, "sleepInfo");
        System.out.println("GOT SLEEP INFO:");
        System.out.println(result);
        sleepService.saveMultipleDaysSleepInfo(result, user.getId());
        System.out.println("Finished saving sleep info!");
    }


    private void saveReadinessInfo(User user, String startDate, String endDate) {
        String result = getResult(user, startDate, endDate, "readinessInfo");
        System.out.println("GOT READINESS INFO:");
        System.out.println(result);
        readinessService.saveMultipleDaysReadinessInfo(result, user.getId());
        System.out.println("Finished saving readiness info!");
    }

    private void saveActivityInfo(User user, String startDate, String endDate) {
        String result = getResult(user, startDate, endDate, "activityInfo");
        System.out.println("GOT ACTIVITY INFO:");
        System.out.println(result);
        activityService.saveMultipleDaysActivityInfo(result, user.getId());
        System.out.println("Finished saving activity info!");
    }

    private String getResult(User user, String startDate, String endDate, String sleepInfo) {
        return new RestTemplate().exchange(env.getProperty(sleepInfo) + "?start=" + startDate + "&end=" + endDate, HttpMethod.GET, getHeader(user.getAccessToken()), String.class).getBody();
    }
    protected AuthorizationCodeResourceDetails oAuthDetails() {
        AuthorizationCodeResourceDetails authorizationCodeResourceDetails = new AuthorizationCodeResourceDetails();
        authorizationCodeResourceDetails.setClientId(env.getProperty(CLIENT_ID));
        authorizationCodeResourceDetails.setClientSecret(env.getProperty(CLIENT_SECRET));
        authorizationCodeResourceDetails.setAccessTokenUri(env.getProperty(ACCESS_TOKEN_URI));
        authorizationCodeResourceDetails.setUserAuthorizationUri(env.getProperty(AUTHORIZATION_URI));
        authorizationCodeResourceDetails.setGrantType(env.getProperty(GRANT_TYPE));
        return authorizationCodeResourceDetails;
    }

    private HttpEntity<String> getHeader(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + token);
        return new HttpEntity<>(headers);
    }

    private String getUserTypeAsString(User user) {

        if (user.getIsSwProfileCompleted()) {
            if (user.getIsSwimmer() && user.getIsCoach()) {
                return "Swimmer+Coach";
            } else if (user.getIsSwimmer()) {
                return "Swimmer";
            } else if (user.getIsCoach()) {
                return "Coach";
            } else {
                return "Nothing";
            }
        } else {
            if (user.getIsSwimmer() && user.getIsCoach()) {
                return "SwimmerIncompleteProfile+Coach";
            } else if (user.getIsSwimmer()) {
                return "SwimmerIncompleteProfile";
            } else if (user.getIsCoach()) {
                return "Coach";
            } else {
                return "Nothing";
            }
        }
    }

    @Scheduled(cron = UPDATE_TOKEN_SCHEDULER)
    public void scheduledTokenUpdate() {
        userService.getUsers().forEach(this::renewAccessToken);
    }


    @Scheduled(cron = UPDATE_OURA_DATA_SCHEDULER)
    public void scheduleTaskUsingCronExpression() {
        userService.getUsers().forEach(this::grabLastOuraData);
    }

}
