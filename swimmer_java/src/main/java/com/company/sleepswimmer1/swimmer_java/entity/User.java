package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::user\"")
public class User {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"email\"")
    private String email;
    
    @Column(name="\"first_name\"")
    private String firstName;
    
    @Column(name="\"last_name\"")
    private String lastName;
    
    @Column(name="\"priority\"")
    private int priority;
    
    @Column(name="\"is_swimmer\"")
    private boolean isSwimmer;
    
    @Column(name="\"is_coach\"")
    private boolean isCoach;
    
    @Column(name="\"oura_allowed\"")
    private boolean ouraAllowed;
    
    @Column(name="\"access_token\"")
    private String accessToken;
    
    @Column(name="\"expires_in\"")
    private int expiresIn;
    
    @Column(name="\"refresh_token\"")
    private String refreshToken;

    @Column(name="\"last_updated\"")
    private String lastUpdated;

	@Column(name="\"is_sw_profile_completed\"")
    private boolean isSwProfileCompleted;
    
	@Column(name="\"sw_profile_name\"")
    private String swProfileName;
    
    @Column(name="\"sw_profile_last_name\"")
    private String swProfileLastName;
    
    @Column(name="\"sw_profile_birthday\"")
    private LocalDate swProfileBirthday;
    
    @Column(name="\"sw_profile_gender\"")
    private String swProfileGender;
    
    @Column(name="\"sw_profile_height\"")
    private int swProfileHeight;
    
    @Column(name="\"sw_profile_weight\"")
    private int swProfileWeight;
    
    @Column(name="\"sw_profile_speciality_stroke\"")
    private String swProfileSpecialityStroke;
    
    @Column(name="\"sw_profile_speciality_distance\"")
    private int swProfileSpecialityDistance;
    
    @Column(name="\"sw_profile_speciality_reference_strokes\"")
    private int swProfileSpecialityReferenceStrokes;
	
    public User() {
    }
    
    public User(String id, String email, String firstName, String lastName, int priority, boolean isSwimmer, boolean isCoach,
    	boolean ouraAllowed, String accessToken,int expiresIn, String refreshToken) {
    	this.id = id;
    	this.email = email;
    	this.firstName = firstName;
    	this.lastName = lastName;
    	this.priority = priority;
    	this.isSwimmer = isSwimmer;
    	this.isCoach = isCoach;
    	this.ouraAllowed = ouraAllowed;
    	this.accessToken = accessToken;
    	this.expiresIn = expiresIn;
    	this.refreshToken = refreshToken;
    }
    
    public User(String firstName, String lastName, int priority) {
    	this.firstName = firstName;
    	this.lastName = lastName;
    	this.priority = priority;
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
    public String getId() {
    	return id;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }
    
    public String getEmail() {
    	return email;
    }
    
    public void setFirstName(String firstName) {
    	this.firstName = firstName;
    }
    
    public String getFirstName() {
    	return firstName;
    }
    
    public void setLastName(String lastName) {
    	this.lastName = lastName;
    }
    
    public String getLastName() {
    	return lastName;
    }
    
    public void setPriority(int priority) {
    	this.priority = priority;
    }
    
    public int getPriority() {
    	return priority;
    }
    
    public void setIsSwimmer(boolean isSwimmer) {
    	this.isSwimmer = isSwimmer;
    }
    
    public boolean getIsSwimmer() {
    	return isSwimmer;
    }
    
    public void setIsCoach(boolean isCoach) {
    	this.isCoach = isCoach;
    }
    
    public boolean getIsCoach() {
    	return isCoach;
    }
    
    public void setOuraAllowed(boolean ouraAllowed) {
    	this.ouraAllowed = ouraAllowed;
    }
    
    public boolean getOuraAllowed() {
    	return ouraAllowed;
    }
    
    public void setAccessToken(String accessToken) {
    	this.accessToken = accessToken;
    }
    
    public String getAccessToken() {
    	return accessToken;
    }
    
    public void setExpiresIn(int expiresIn) {
    	this.expiresIn = expiresIn;
    }
    
    public int getExpiresIn() {
    	return expiresIn;
    }
    
    public void setRefreshToken(String refreshToken) {
    	this.refreshToken = refreshToken;
    }
    
    public String getRefreshToken() {
    	return refreshToken;
    }
    
    public void setIsSwProfileCompleted(boolean isSwProfileCompleted) {
    	this.isSwProfileCompleted = isSwProfileCompleted;
    }
    
    public boolean getIsSwProfileCompleted() {
    	return isSwProfileCompleted;
    }
    
    public String getSwProfileName() {
    	return swProfileName;
    }
    public void setSwProfileName(String swProfileName) {
    	this.swProfileName = swProfileName;
    }
    
    public String getSwProfileLastName() {
    	return swProfileLastName;
    }
    public void setSwProfileLastName(String swProfileLastName) {
    	this.swProfileLastName = swProfileLastName;
    }
    
    public LocalDate getSwProfileBirthday() {
    	return swProfileBirthday;
    }
    public void setSwProfileBirthday(LocalDate swProfileBirthday) {
    	this.swProfileBirthday = swProfileBirthday;
    }
    
    public String getSwProfileGender() {
    	return swProfileGender;
    }
    public void setSwProfileGender(String swProfileGender) {
    	this.swProfileGender = swProfileGender;
    }
    
    public int getSwProfileHeight() {
    	return swProfileHeight;
    }
    public void setSwProfileHeight(int swProfileHeight) {
    	this.swProfileHeight = swProfileHeight;
    }
    
    public int getSwProfileWeight() {
    	return swProfileWeight;
    }
    public void setSwProfileWeight(int swProfileWeight) {
    	this.swProfileWeight = swProfileWeight;
    }
    
    public String getSwProfileSpecialityStroke() {
    	return swProfileSpecialityStroke;
    }
    public void setSwProfileSpecialityStroke(String swProfileSpecialityStroke) {
    	this.swProfileSpecialityStroke = swProfileSpecialityStroke;
    }
    
    public int getSwProfileSpecialityDistance() {
    	return swProfileSpecialityDistance;
    }
    public void setSwProfileSpecialityDistance(int swProfileSpecialityDistance) {
    	this.swProfileSpecialityDistance = swProfileSpecialityDistance;
    }
    
    public int getSwProfileSpecialityReferenceStrokes() {
    	return swProfileSpecialityReferenceStrokes;
    }
    public void setSwProfileSpecialityReferenceStrokes(int swProfileSpecialityReferenceStrokes) {
    	this.swProfileSpecialityReferenceStrokes = swProfileSpecialityReferenceStrokes;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
