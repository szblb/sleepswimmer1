package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::hypnogram_5min\"")
public class Hypnogram5Min {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"hypnogram_5min_id\"")
    private String hypnogram5MinId;
    
    @Column(name="\"hypnogram_5min_value\"")
    private int hypnogram5MinValue;
    
    @Column(name="\"hypnogram_5min_start\"")
    private ZonedDateTime hypnogram5MinStart;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getHypnogram5MinId() {
        return hypnogram5MinId;
    }

    public void setHypnogram5MinId(String hypnogram5MinId) {
        this.hypnogram5MinId = hypnogram5MinId;
    }
    
    public int getHypnogram5MinValue() {
        return hypnogram5MinValue;
    }

    public void setHypnogram5MinValue(int hypnogram5MinValue) {
        this.hypnogram5MinValue = hypnogram5MinValue;
    }
    
    public ZonedDateTime getHypnogram5MinStart() {
        return hypnogram5MinStart;
    }

    public void setHypnogram5MinStart(ZonedDateTime hypnogram5MinStart) {
        this.hypnogram5MinStart = hypnogram5MinStart;
    }
}
