package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::met_1min\"")
public class Met1Min {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"met_1min_id\"")
    private String met1MinId;
    
    @Column(name="\"met_1min_value\"")
    private double met1MinValue;
    
    @Column(name="\"met_1min_start\"")
    private ZonedDateTime met1MinStart;
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getMet1MinId() {
        return met1MinId;
    }

    public void setMet1MinId(String met1MinId) {
        this.met1MinId = met1MinId;
    }
    
    public double getMet1MinValue() {
        return met1MinValue;
    }

    public void setMet1MinValue(double met1MinValue) {
        this.met1MinValue = met1MinValue;
    }
    
    public ZonedDateTime getMet1MinStart() {
        return met1MinStart;
    }

    public void setMet1MinStart(ZonedDateTime met1MinStart) {
        this.met1MinStart = met1MinStart;
    }
}
