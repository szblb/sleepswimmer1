package com.company.sleepswimmer1.swimmer_java.repository;

import com.company.sleepswimmer1.swimmer_java.entity.Met1Min;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Met1MinRepository extends CrudRepository<Met1Min, String> {
}
