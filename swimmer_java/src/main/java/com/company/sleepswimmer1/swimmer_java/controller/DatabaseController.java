package com.company.sleepswimmer1.swimmer_java.controller;

import com.company.sleepswimmer1.swimmer_java.entity.Activity;
import com.company.sleepswimmer1.swimmer_java.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;


//TODO this is not used currently, refactor/delete
@Controller
@RequestMapping(path="/database")
public class DatabaseController {
    @Autowired
    private Environment env;

    @Autowired 
    ActivityService activityService;

    @PostMapping(path="/addActivity") // Map ONLY POST Requests
    public @ResponseBody String addActivity(/*@RequestParam String id*/) {

        //Activity newActivity = new Activity();
        //activityRepository.save(newActivity);

        return "Saved";
    }

    @GetMapping(path="/getAllActivity")
    public @ResponseBody Iterable<Activity> getAllActivity() {
        // This returns a JSON or XML with the areas
        //return activityRepository.findAll();
        return new ArrayList<Activity>();
    }

    @GetMapping(value = "/activityInfo")
    ResponseEntity<String> getDashboard(@RequestParam String token) {
        ResponseEntity<String> result = new RestTemplate().exchange(env.getProperty("activityInfo"), HttpMethod.GET, getHeader(token), String.class);
        //activityService.saveActivityInfo(result.getBody());

        return result;
    }

    private HttpEntity<String> getHeader(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + token);
        return new HttpEntity<>(headers);
    }
}