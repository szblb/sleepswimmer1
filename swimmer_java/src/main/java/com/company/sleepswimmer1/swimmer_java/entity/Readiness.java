package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::readiness\"")
public class Readiness {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"summary_date\"")
    private LocalDate summaryDate;
    
    @Column(name="\"period_id\"")
    private int periodId;
    
    @Column(name="\"score\"")
    private int score;
    
    @Column(name="\"score_previous_night\"")
    private int scorePreviousNight;
    
    @Column(name="\"score_sleep_balance\"")
    private int scoreSleepBalance;
    
    @Column(name="\"score_previous_day\"")
    private int scorePreviousDay;
    
    @Column(name="\"score_activity_balance\"")
    private int scoreActivityBalance;
    
    @Column(name="\"score_resting_hr\"")
    private int scoreRestingHr;
    
    @Column(name="\"score_recovery_index\"")
    private int scoreRecoveryIndex;
    
    @Column(name="\"score_temperature\"")
    private int scoreTemperature;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public LocalDate getSummaryDate() {
        return summaryDate;
    }

    public void setSummaryDate(LocalDate summaryDate) {
        this.summaryDate = summaryDate;
    }
    
    public void setSummaryDate(String summaryDate) {
        this.summaryDate = LocalDate.parse(summaryDate);
    }
    
    public int getPeriodId() {
        return periodId;
    }

    public void setPeriodId(int periodId) {
        this.periodId = periodId;
    }
    
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    public int getScorePreviousNight() {
        return scorePreviousNight;
    }

    public void setScorePreviousNight(int scorePreviousNight) {
        this.scorePreviousNight = scorePreviousNight;
    }
    
    public int getScoreSleepBalance() {
        return scoreSleepBalance;
    }

    public void setScoreSleepBalance(int scoreSleepBalance) {
        this.scoreSleepBalance = scoreSleepBalance;
    }
    
    public int getScorePreviousDay() {
        return scorePreviousDay;
    }

    public void setScorePreviousDay(int scorePreviousDay) {
        this.scorePreviousDay = scorePreviousDay;
    }
    
    public int getScoreActivityBalance() {
        return scoreActivityBalance;
    }

    public void setScoreActivityBalance(int scoreActivityBalance) {
        this.scoreActivityBalance = scoreActivityBalance;
    }
    
    public int getScoreRestingHr() {
        return scoreRestingHr;
    }

    public void setScoreRestingHr(int scoreRestingHr) {
        this.scoreRestingHr = scoreRestingHr;
    }
    
    public int getScoreRecoveryIndex() {
        return scoreRecoveryIndex;
    }

    public void setScoreRecoveryIndex(int scoreRecoveryIndex) {
        this.scoreRecoveryIndex = scoreRecoveryIndex;
    }
    
    public int getScoreTemperature() {
        return scoreTemperature;
    }

    public void setScoreTemperature(int scoreTemperature) {
        this.scoreTemperature = scoreTemperature;
    }
}
