package com.company.sleepswimmer1.swimmer_java.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name="\"sleepswimmer1.db::coachdailysurvey\"")
public class CoachDailySurvey {
	@Id
    @Column(name="\"dw_id\"")
    private String id = UUID.randomUUID().toString();
    
    @Column(name="\"user_id\"")
    private String userId;
    
    @Column(name="\"survey_day\"")
    private LocalDate surveyDay;
    
    @Column(name="\"schedule_name\"")
    private String scheduleName;
    
    @Column(name="\"swimming_distance\"")
    private double swimmingDistance;
    
    @Column(name="\"training_intensity\"")
    private String trainingIntensity;
    
    @Column(name="\"duration\"")
    private double duration;
	
	@Column(name="\"comments\"")
    private String comments;
    
    public CoachDailySurvey() {
    }
    
    public void setId(String id) {
    	this.id = id;
    }
    
    public String getId() {
    	return id;
    }
    
    public void setUserId(String userId) {
    	this.userId = userId;
    }
    
    public String getUserId() {
    	return userId;
    }
    
    public void setSurveyDay(LocalDate surveyDay) {
    	this.surveyDay = surveyDay;
    }
    
    public LocalDate getSurveyDay() {
    	return surveyDay;
    }
    
    public void setScheduleName(String scheduleName) {
    	this.scheduleName = scheduleName;
    }
    
    public String getScheduleName() {
    	return scheduleName;
    }
    
    public void setSwimmingDistance(double swimmingDistance) {
    	this.swimmingDistance = swimmingDistance;
    }
    
    public double getSwimmingDistance() {
    	return swimmingDistance;
    }
    
    public void setTrainingIntensity(String trainingIntensity) {
    	this.trainingIntensity = trainingIntensity;
    }
    
    public String getTrainingIntensity() {
    	return trainingIntensity;
    }
    
    public void setDuration(double duration) {
    	this.duration = duration;
    }
    
    public double getDuration() {
    	return duration;
    }
    
    public void setComments(String comments) {
    	this.comments = comments;
    }
    
    public String getComments() {
    	return comments;
    }
}
