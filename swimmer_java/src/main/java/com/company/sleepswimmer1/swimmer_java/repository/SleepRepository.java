package com.company.sleepswimmer1.swimmer_java.repository;

import com.company.sleepswimmer1.swimmer_java.entity.Sleep;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SleepRepository extends CrudRepository<Sleep, String> {
}
